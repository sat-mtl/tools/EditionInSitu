from unittest import TestCase, mock

from satnet.serialization import Serializable
from .mock.sync import MockSyncEntity, MockSyncEntityA, MockSyncEntityB


class TestSyncEntity(TestCase):

    def test_sync_inheritance(self):
        entity_a = MockSyncEntityA()
        self.assertIn(MockSyncEntityA, list(entity_a._syncs.keys()))
        self.assertEqual(['_test', '_test_a'], list(entity_a._syncs[MockSyncEntityA].keys()))

        entity_b = MockSyncEntityB()
        self.assertIn(MockSyncEntityB, list(entity_a._syncs.keys()))
        self.assertEqual(['_test', '_test_b'], list(entity_b._syncs[MockSyncEntityB].keys()))

    def test_uuid_serialization(self):
        entity = MockSyncEntity()
        entity_prime = Serializable.deserialize(entity.serialize())
        self.assertEqual(entity.uuid, entity_prime.uuid)

    def test_sync_stores_values_in_owner_instance(self):
        entity1 = MockSyncEntity()
        entity1.test = "Pouet"
        entity2 = MockSyncEntity()
        entity2.test = "Poupou"
        self.assertEqual("Pouet", entity1.test)
        self.assertEqual("Poupou", entity2.test)

    def test_sync_serializes(self):
        entity = MockSyncEntity()
        entity.test = "Pouet"
        entity_prime = Serializable.deserialize(entity.serialize())
        self.assertEqual("Pouet", entity_prime.test)

    def test_sync_change_callback(self):
        entity = MockSyncEntity()
        with mock.patch.object(entity, "on_sync_changed") as osc:
            entity.test = "Coucou"
            osc.assert_called_once_with(entity, vars(MockSyncEntity)['test'], "Coucou")
