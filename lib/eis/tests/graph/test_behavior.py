from unittest import TestCase, mock

from eis.graph.behavior import Behavior
from eis.graph.model import Model
from eis.graph.object_3d import Object3D


class TestBehavior(TestCase):
    def setUp(self):
        self._object = Object3D()
        self._otherObject = Object3D()
        self._model = Model()
        self._model.root.add_child(self._object)
        self._model.root.add_child(self._otherObject)
        self._behavior = Behavior()

    def test_added_to_model(self):
        with mock.patch.object(self._model, "add_behavior") as added_to_model:
            self._object.add_behavior(self._behavior)
            added_to_model.assert_called_once_with(self._behavior)

        with mock.patch.object(self._model, "add_behavior") as added_to_model, \
                mock.patch.object(self._model, "remove_behavior") as removed_from_model:
            self._otherObject.add_behavior(self._behavior)
            added_to_model.assert_called_once_with(self._behavior)
            removed_from_model.assert_called_once_with(self._behavior)

    def test_removed_from_model(self):
        self._object.add_behavior(self._behavior)
        self._otherObject.add_behavior(self._behavior)

        with mock.patch.object(self._behavior, "removed_from_model") as removed_from_model:
            self._object.remove_behavior(self._behavior)
            removed_from_model.assert_called_once_with(self._model)

        with mock.patch.object(self._behavior, "removed_from_model") as removed_from_model:
            self._otherObject.remove_behavior(self._behavior)
            removed_from_model.assert_not_called()
