import uuid
from unittest import TestCase, mock

from eis.graph.geometry import Geometry
from eis.graph.material import Material
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from satmath.matrix44 import Matrix44


class TestGeometry(TestCase):
    def test_copy(self):
        material = Material()
        original = Geometry(
            name="geometry",
            vertices=[(1, 2, 3), (4, 5, 6)],
            primitives=[[1, 2, 3], [4, 5, 6]],
            primitive_type=Geometry.PrimitiveType.TRIANGLE_STRIP,
            material=material,
            material_id=uuid.uuid4()
        )

        with mock.patch.object(material, "copy", wraps=material.copy) as material_copy:
            copy = original.copy()
            material_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertEqual(original.vertices, copy.vertices)
        self.assertIsNot(original.vertices, copy.vertices)
        self.assertEqual(original.primitives, copy.primitives)
        self.assertIsNot(original.primitives, copy.primitives)
        self.assertEqual(original.primitive_type, copy.primitive_type)

        self.assertNotEqual(original.material, copy.material)
        self.assertNotEqual(original.material_id, copy.material_id)
        self.assertEqual(copy.material.uuid, copy.material_id)

    def test_copy_unless_managed(self):
        material = Material()
        material.managed = True
        original = Geometry(material=material)

        with mock.patch.object(material, "copy", wraps=material.copy) as material_copy:
            copy = original.copy()
            material_copy.assert_not_called()

        self.assertIsNone(copy._material)
        self.assertIsNone(copy._material_id)

    def test_copy_shared(self):
        material = Material()
        original = Geometry(
            name="geometry",
            vertices=[(1, 2, 3), (4, 5, 6)],
            primitives=[[1, 2, 3], [4, 5, 6]],
            primitive_type=Geometry.PrimitiveType.TRIANGLE_STRIP,
            material=material,
            material_id=uuid.uuid4()
        )

        copy = original.copy_shared()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertEqual(original.vertices, copy.vertices)
        self.assertIsNot(original.vertices, copy.vertices)
        self.assertEqual(original.primitives, copy.primitives)
        self.assertIsNot(original.primitives, copy.primitives)
        self.assertEqual(original.primitive_type, copy.primitive_type)

        self.assertEqual(original.material, copy.material)
        self.assertEqual(original.material_id, copy.material_id)

    def test_copy_shared_unless_managed(self):
        material = Material()
        material.managed = True
        original = Geometry(material=material)

        copy = original.copy_shared()
        self.assertIsNone(copy._material)
        self.assertIsNone(copy._material_id)

    def test_material_property_returns_default_material(self):
        """Test the material property method returns the default material if
        no material is set.
        """
        geometry = Geometry()
        self.assertIsNone(geometry._material)
        self.assertEqual(geometry.material, Geometry._default_material)

    def test_material_setter(self):
        geometry = Geometry()
        self.assertIsNone(geometry._material)
        material = Material()
        geometry.material = material
        self.assertEqual(geometry._material, material)

    def test_material_setter_sets_material_id(self):
        """Test the material setter method also updates the material_id attribute."""
        geometry = Geometry()
        self.assertIsNone(geometry._material_id)
        material = Material()
        geometry.material = material
        self.assertEqual(geometry._material_id, material.uuid)

    def test_material_setter_updates_model(self):
        """Test setting a new material adds it to the model and removes the
        previous material from the model.
        """
        model = Model()
        mesh = Mesh()
        model.add_mesh(mesh)

        material = Material()
        geometry = Geometry(material=material)
        mesh.add_geometry(geometry)

        self.assertTrue(material in model.materials)
        self.assertTrue(model in material.owners.keys())

        new_material = Material()
        geometry.material = new_material

        self.assertTrue(new_material in model.materials)
        self.assertTrue(model in new_material.owners.keys())
        self.assertFalse(material in model.materials)
        self.assertFalse(model in material.owners.keys())

    def test_material_setter_updates_material_geometries(self):
        """Test setting a new material updates the material.geometries attribute."""
        material = Material()
        geometry = Geometry(material=material)

        self.assertTrue(geometry in material.geometries)

        new_material = Material()
        geometry.material = new_material

        self.assertFalse(geometry in material.geometries)
        self.assertTrue(geometry in new_material.geometries)

    def test_material_setter_updates_object3d(self):
        """Test setting a new material adds it to the object3d and removes the
        previous material from the object3d.
        """
        mesh = Mesh()
        object3d = Object3D(mesh=mesh)

        material = Material()
        geometry = Geometry(material=material)
        mesh.add_geometry(geometry)

        self.assertTrue(material in object3d.materials)

        new_material = Material()
        geometry.material = new_material

        self.assertTrue(new_material in object3d.materials)
        self.assertFalse(material in object3d.materials)

    def test_material_id_changed_sets_material(self):
        """Test changing the material_id attribute sets the material."""
        model = Model()
        mesh = Mesh()
        model.add_mesh(mesh)
        material = Material()
        model.add_material(material)

        geometry = Geometry()
        geometry.mesh = mesh
        self.assertIsNone(geometry._material)

        geometry.material_id = material.uuid
        self.assertEqual(geometry._material, material)


class TestObject3D(TestCase):
    def test_copy(self):
        child_mesh = Mesh()
        child = Object3D(mesh=child_mesh)
        parent_mesh = Mesh()
        original = Object3D(
            name="object",
            children=[child],
            matrix=Matrix44((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)),
            mesh=parent_mesh
        )

        with mock.patch.object(child, "copy", wraps=child.copy) as child_copy, \
                mock.patch.object(child_mesh, "copy", wraps=child_mesh.copy) as child_mesh_copy, \
                mock.patch.object(parent_mesh, "copy", wraps=parent_mesh.copy) as parent_mesh_copy:
            copy = original.copy()
            child_copy.assert_called_once_with()
            child_mesh_copy.assert_called_once_with()
            parent_mesh_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertNotEqual(original.children, copy.children)
        self.assertNotEqual(original.children[0].mesh, copy.children[0].mesh)
        self.assertEqual(original.matrix, copy.matrix)
        self.assertIsNot(original.matrix, copy.matrix)
        self.assertNotEqual(original.mesh, copy.mesh)

    def test_copy_unless_managed(self):
        child = Object3D()
        child.managed = True
        mesh = Mesh()
        mesh.managed = True
        original = Object3D(mesh=mesh, children=[child])

        with mock.patch.object(mesh, "copy", wraps=mesh.copy) as mesh_copy, \
                mock.patch.object(child, "copy", wraps=child.copy) as child_copy:
            copy = original.copy()
            mesh_copy.assert_not_called()
            child_copy.assert_not_called()

        self.assertIsNone(copy.mesh)
        self.assertIsNone(copy.mesh_id)
        self.assertEqual(len(copy.children), 0)

    def test_copy_shared(self):
        child_mesh = Mesh()
        child = Object3D(mesh=child_mesh)
        parent_mesh = Mesh()
        original = Object3D(
            name="object",
            children=[child],
            matrix=Matrix44((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)),
            mesh=parent_mesh
        )

        with mock.patch.object(child, "copy_shared", wraps=child.copy_shared) as child_copy:
            copy = original.copy_shared()
            child_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertNotEqual(original.children, copy.children)
        self.assertEqual(original.children[0].mesh, copy.children[0].mesh)
        self.assertIs(original.children[0].mesh, copy.children[0].mesh)
        self.assertEqual(original.matrix, copy.matrix)
        self.assertIsNot(original.matrix, copy.matrix)
        self.assertEqual(original.mesh, copy.mesh)
        self.assertIs(original.mesh, copy.mesh)

    def test_copy_shared_unless_managed(self):
        child = Object3D()
        child.managed = True
        mesh = Mesh()
        mesh.managed = True
        original = Object3D(mesh=mesh, children=[child])

        with mock.patch.object(child, "copy_shared", wraps=child.copy_shared) as child_copy:
            copy = original.copy_shared()
            child_copy.assert_not_called()
        self.assertIsNone(copy.mesh)
        self.assertIsNone(copy.mesh_id)
        self.assertEqual(len(copy.children), 0)

    def test_setting_mesh_no_model(self):
        geom = Object3D()
        mesh = Mesh()

        with mock.patch.object(mesh, "removed_from_model") as removed_model, \
                mock.patch.object(mesh, "added_to_model") as added_model:
            geom.mesh = mesh
            removed_model.assert_not_called()
            added_model.assert_not_called()

    def test_setting_mesh_with_model(self):
        model = Model()
        geom = Object3D()
        geom._model = model
        mesh = Mesh()

        with mock.patch.object(mesh, "removed_from_model") as removed_model, \
                mock.patch.object(mesh, "added_to_model") as added_model:
            geom.mesh = mesh
            removed_model.assert_not_called()
            added_model.assert_called_once_with(model)

    def test_switching_mesh_with_no_model(self):
        mesh1 = Mesh()
        geom = Object3D(mesh=mesh1)
        mesh2 = Mesh()

        with mock.patch.object(mesh1, "removed_from_model") as removed_model1, \
                mock.patch.object(mesh1, "added_to_model") as added_model1, \
                mock.patch.object(mesh2, "removed_from_model") as removed_model2, \
                mock.patch.object(mesh2, "added_to_model") as added_model2:
            geom.mesh = mesh2
            removed_model1.assert_not_called()
            added_model1.assert_not_called()
            removed_model2.assert_not_called()
            added_model2.assert_not_called()

    def test_switching_mesh_with_model(self):
        model = Model()
        mesh1 = Mesh()
        geom = Object3D(mesh=mesh1)
        geom._model = model
        mesh2 = Mesh()

        with mock.patch.object(mesh1, "removed_from_model") as removed_model1, \
                mock.patch.object(mesh1, "added_to_model") as added_model1, \
                mock.patch.object(mesh2, "removed_from_model") as removed_model2, \
                mock.patch.object(mesh2, "added_to_model") as added_model2:
            geom.mesh = mesh2
            removed_model1.assert_called_once_with(model)
            added_model1.assert_not_called()
            removed_model2.assert_not_called()
            added_model2.assert_called_once_with(model)

    def test_retrieves_mesh_when_added_to_model(self):
        model = Model()
        obj = Object3D(mesh_id=uuid.uuid4())
        with mock.patch.object(model, "get_mesh_by_uuid", wraps=model.get_mesh_by_uuid) as get:
            model.root.add_child(obj)
            get.assert_called_once_with(obj.mesh_id)

    def test_does_not_retrieve_mesh_when_added_to_model(self):
        model = Model()
        obj = Object3D()
        with mock.patch.object(model, "get_mesh_by_uuid", wraps=model.get_mesh_by_uuid) as get:
            model.root.add_child(obj)
            get.assert_not_called()

    def test_propagates_removal_from_model(self):
        model = Model()
        mesh = Mesh()
        obj = Object3D(mesh=mesh)
        model.root.add_child(obj)
        with mock.patch.object(mesh, "removed_from_model") as remove:
            model.root.remove_child(obj)
            remove.assert_called_once_with(model)
