from unittest import TestCase, mock

from eis.graph.behavior import Behavior
from eis.graph.base import GraphBase
from eis.graph.model import Model
from eis.graph.texture import Texture


class TestBehavior(Behavior):
    def can_control(self, graphbase: GraphBase) -> bool:
        if isinstance(graphbase, Texture):
            return True
        return False


class TestTexture(TestCase):
    def test_copy(self):
        original = Texture(
            name="some texture",
            data=b'pouetpoupou',
            format="bigly",
            size_x=69,
            size_y=420,
            component_type=Texture.ComponentType.UNKNOWN,
            compression=Texture.Compression.DXT5
        )

        copy = original.copy()

        self.assertIsNone(copy.proxy)
        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertEqual(original.data, copy.data)
        self.assertEqual(original.format, copy.format)
        self.assertEqual(original.size_x, copy.size_x)
        self.assertEqual(original.size_y, copy.size_y)
        self.assertEqual(original.component_type, copy.component_type)
        self.assertEqual(original.compression, copy.compression)

    def test_copy_shared(self):
        original = Texture(
            name="some texture",
            data=b'pouetpoupou',
            format="bigly",
            size_x=69,
            size_y=420,
            component_type=Texture.ComponentType.UNKNOWN,
            compression=Texture.Compression.DXT5
        )

        copy = original.copy_shared()

        self.assertIsNone(copy.proxy)
        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertEqual(original.data, copy.data)
        self.assertEqual(original.format, copy.format)
        self.assertEqual(original.size_x, copy.size_x)
        self.assertEqual(original.size_y, copy.size_y)
        self.assertEqual(original.component_type, copy.component_type)
        self.assertEqual(original.compression, copy.compression)

    def test_added_to_model(self):
        model = Model()
        texture = Texture()
        with mock.patch.object(model, "add_texture") as add:
            texture.added_to_model(model)
            add.assert_called_once_with(texture)

    def test_removed_from_model(self):
        model = Model()
        texture = Texture()
        with mock.patch.object(model, "remove_texture") as remove:
            texture.removed_from_model(model)
            remove.assert_called_once_with(texture)

    def test_texture_behavior(self):
        model = Model()
        texture = Texture()
        behavior = TestBehavior()

        texture.add_behavior(behavior)
        self.assertIn(behavior, texture._synced_behaviors)
        self.assertEqual(behavior.graphbase, texture)

        texture.added_to_model(model)
        self.assertEqual(behavior.owners[model], 1)
        self.assertIn(behavior.uuid, model._behavior_map)

        texture.remove_behavior(behavior)
        self.assertNotIn(behavior, texture._synced_behaviors)
        self.assertEqual(behavior.owners, {})
