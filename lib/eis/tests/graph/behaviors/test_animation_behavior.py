from ...mock.editor import MockEditor_extended

# Important to set the value of CLIENT here
# It needs to be define before the other imports
import eis.constants
eis.constants.CLIENT = True

from eis.graph.scene import Scene
from eis.graph.object_3d import Object3D
from eis.graph.model import Model
from eis.graph.behaviors.animation_behavior import AnimationBehavior
from eis.graph.animation_curve import AnimationCurve
from unittest import TestCase

class MockModel(Model):
    def __init__(self):
        super().__init__()


class MockScene(Scene):
    def __init__(self):
        super().__init__(model=MockModel())


class MockObject3D(Object3D):
    def __init__(self):
        super().__init__()


class EditorAnimationBehavior(MockEditor_extended):
    def __init__(self):
        super().__init__()
        defaultEditorConfig = {"timeline.delay_before_loop": 10.0, "timeline.looping": True, "timeline.duration": 60}
        self._config = defaultEditorConfig


class TestAnimationBehavior(TestCase):

    def setUp(self):
        first_curve = AnimationCurve(keyframes=[(0.0, 0.0), (1.0, 1.0), (2.0, 2.0)])
        second_curve = AnimationCurve(keyframes=[(0.5, 0.5), (1.5, 1.5), (2.0, 2.0), (2.5, 2.5)])
        first_curve.initialize()
        second_curve.initialize()
        self._behavior = AnimationBehavior(curves={"first": first_curve, "second": second_curve})

    def test_initialize(self):
        self.assertEqual(self._behavior._previous_timeline_position, -1.0)
        self.assertEqual(self._behavior._keyframe_times, [])
        self.assertEqual(self._behavior._last_curve_timestamp, 0.0)
        self.assertIsNone(self._behavior._selected_keyframe)
        self.assertIsNone(self._behavior._delay_before_loop)

    def test_update_curves(self):
        # add a curve to a model and the model to the behavior
        third_curve = AnimationCurve(keyframes=[(0.0, 0.0), (1.0, 1.0), (2.0, 2.0)])
        model = MockModel()
        model.add_animation_curve(animation_curve=third_curve)
        self._behavior.add_owner(model)
        # to test this function, I need to artificially add the curve
        # to the curves_id of the behavior
        self._behavior._curves_id["third"] = third_curve.uuid
        self._behavior._update_curves()

        # make sure the curve has been added to the behavior
        self.assertEqual(self._behavior._curves["third"],
                         model._animation_curve_map[self._behavior._curves_id["third"]])

    def test_add_owner(self):
        model = MockModel()
        self._behavior.add_owner(model)
        # verify that the animation curves of the behavior matches with the ones from the model
        self.assertEqual(self._behavior._curves["first"],
                         model._animation_curve_map[self._behavior._curves_id["first"]])
        self.assertEqual(self._behavior._curves["second"],
                         model._animation_curve_map[self._behavior._curves_id["second"]])

    def test_remove_owner(self):
        model = MockModel()
        self._behavior.add_owner(model)
        # remove yourself from the model
        self._behavior.remove_owner(model)
        # check that the animation curves have been removed as well
        self.assertNotIn(self._behavior._curves_id["first"], model._animation_curve_map)
        self.assertNotIn(self._behavior._curves_id["second"], model._animation_curve_map)

    def test_update_keyframe_times(self):
        self._behavior.initialize()
        self._behavior._update_keyframe_times()
        self.assertEqual(self._behavior._last_curve_timestamp, self._behavior._curves["second"].timestamp)
        self.assertEqual(self._behavior._keyframe_times, [0.0, 0.5, 1.0, 1.5, 2.0, 2.5])

    def test_update_selected_keyframe(self):
        self._behavior._update_keyframe_times()
        self._behavior._update_selected_keyframe(timeline_position=1.4)
        self.assertEqual(self._behavior._selected_keyframe, 1.5)

    def test_step(self):
        object3d = MockObject3D()
        scene = MockScene()
        editor = EditorAnimationBehavior()
        scene.editor = editor
        object3d.scene = scene
        self._behavior.graphbase = object3d
        # set a location
        object3d.x = 10.0
        object3d.y = 4.0
        object3d.z = 4.0

        # create an animation curve with a valid attribute
        self._behavior.add_timed_keyframe_with_value(attribute="location", time=1.0, value=[1.0, 1.0, 1.0])
        self._behavior.add_timed_keyframe_with_value(attribute="location", time=2.0, value=[2.0, 2.0, 2.0])
        # first, step in the timeline via the internal call _step_animation
        # (we don't want to trigger the timeline.step() because it requires us to
        # initialize a machine)
        self._behavior.graphbase.scene.editor.timeline._step_animation(dt=2.0)
        self._behavior.step(dt=2.0, now=0.0)

        self.assertEqual(self._behavior.graphbase.location, [2.0, 2.0, 2.0])

    def test_add_keyframe(self):
        object3d = MockObject3D()
        scene = MockScene()
        editor = EditorAnimationBehavior()
        scene.editor = editor
        object3d.scene = scene
        # set a location. We will test adding a keyframe to this attribute
        object3d.x = 10.0
        object3d.y = 4.0
        object3d.z = 4.0
        self._behavior.graphbase = object3d
        self._behavior.add_keyframe("location")
        self.assertEqual((0.0, [10.0, 4.0, 4.0]), self._behavior._curves["location"].get_keyframe_at(0.0))

    def test_add_timed_keyframe_with_value(self):
        # verify adding a keyframe to an existing curve
        self._behavior.add_timed_keyframe_with_value(attribute="first", time=3.0, value=3.0)
        self.assertIn((3.0, 3.0), self._behavior._curves["first"].keyframes)

        # verify adding a keyframe to a non-existing curve
        self._behavior.add_timed_keyframe_with_value(attribute="third", time=3.0, value=3.0)
        self.assertIn((3.0, 3.0), self._behavior._curves["third"].keyframes)

    def test_set_interpolation_for_attribute(self):
        self._behavior.set_interpolation_for_attribute("first", AnimationCurve.InterpolationType.SMOOTH)
        self.assertTrue(self._behavior._curves["first"]._interpolation, AnimationCurve.InterpolationType.SMOOTH)
        # try a non-existing curve
        self._behavior.set_interpolation_for_attribute("fourth", AnimationCurve.InterpolationType.SMOOTH)

    def test_remove_keyframe(self):
        self.assertIn((2.0, 2.0), self._behavior._curves["first"].keyframes)
        self.assertTrue(self._behavior.remove_keyframe(attribute="first", time=2.0))
        self.assertNotIn((2.0, 2.0), self._behavior._curves["first"].keyframes)

    def test_get_keyframe_for_all_attributes(self):
        self.assertEqual({"first": 2.0, "second": 2.0}, self._behavior.get_keyframe_for_all_attributes(time=2.0))
        self.assertEqual({}, self._behavior.get_keyframe_for_all_attributes(time=2.7))

    def test_remove_keyframe_for_all_attributes(self):
        self.assertIn((2.0, 2.0), self._behavior._curves["first"].keyframes)
        self.assertIn((2.0, 2.0), self._behavior._curves["second"].keyframes)
        self._behavior.remove_keyframe_for_all_attributes(time=2.0)
        self.assertNotIn((2.0, 2.0), self._behavior._curves["first"].keyframes)
        self.assertNotIn((2.0, 2.0), self._behavior._curves["second"].keyframes)

    def test_get_keyframe_times(self):
        times = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5]
        self.assertEqual(times, self._behavior._get_keyframe_times())

    def test_set_model(self):
        some_model = MockModel()
        self._behavior.add_owner(some_model)
        self.assertIsNot(self._behavior.owners, {})

        new_model = MockModel()
        self._behavior.add_owner(new_model)
        # verify that the animation curves of the behavior matches with the ones from the model
        self.assertEqual(self._behavior._curves["first"],
                         new_model._animation_curve_map[self._behavior._curves_id["first"]])
        self.assertEqual(self._behavior._curves["second"],
                         new_model._animation_curve_map[self._behavior._curves_id["second"]])
