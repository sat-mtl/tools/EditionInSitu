import math
import uuid
from unittest import TestCase, mock

from eis.graph.behavior import Behavior
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from satmath.euler import Euler
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3


class TestObject3D(TestCase):

    def setUp(self):
        self.leaf11 = Object3D(name="leaf1.1")
        self.leaf12 = Object3D(name="leaf1.2")
        self.leaf13 = Object3D(name="leaf1.3")
        self.branch1 = Object3D(name="branch1", children=[self.leaf11, self.leaf12, self.leaf13])
        self.leaf21 = Object3D(name="leaf2.1")
        self.leaf22 = Object3D(name="leaf2.2")
        self.leaf23 = Object3D(name="leaf2.3")
        self.branch2 = Object3D(name="branch2", children=[self.leaf21, self.leaf22, self.leaf23])
        self.root = Object3D(name="root", children=[self.branch1, self.branch2])

        self.scene = Scene()
        self.scene.add_child(self.root)
        # self.root.scene = self.scene

        self.detached = Object3D(name="detached")

    def test_copy(self):
        child = Object3D()
        original = Object3D(
            name="object",
            children=[child],
            matrix=Matrix44((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
        )

        with mock.patch.object(child, "copy", wraps=child.copy) as child_copy:
            copy = original.copy()
            child_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertNotEqual(original.children, copy.children)
        self.assertEqual(original.matrix, copy.matrix)
        self.assertIsNot(original.matrix, copy.matrix)

    def test_copy_unless_managed(self):
        child = Object3D()
        child.managed = True
        original = Object3D(children=[child])

        with mock.patch.object(child, "copy", wraps=child.copy) as child_copy:
            copy = original.copy()
            child_copy.assert_not_called()

        self.assertEqual(len(copy.children), 0)

    def test_copy_shared(self):
        child = Object3D()
        original = Object3D(
            name="object",
            children=[child],
            matrix=Matrix44((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
        )

        with mock.patch.object(child, "copy_shared", wraps=child.copy_shared) as child_copy:
            copy = original.copy_shared()
            child_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertNotEqual(original.children, copy.children)
        self.assertEqual(original.matrix, copy.matrix)
        self.assertIsNot(original.matrix, copy.matrix)

    def test_copy_shared_unless_managed(self):
        child = Object3D()
        child.managed = True
        original = Object3D(children=[child])

        with mock.patch.object(child, "copy_shared", wraps=child.copy_shared) as child_copy:
            copy = original.copy_shared()
            child_copy.assert_not_called()

        self.assertEqual(len(copy.children), 0)

    def test_get_by_uuid(self):
        self.assertEqual(self.root.find_by_uuid(self.root.uuid), self.root)
        self.assertEqual(self.root.find_by_uuid(self.branch1.uuid), self.branch1)
        self.assertEqual(self.root.find_by_uuid(self.leaf11.uuid), self.leaf11)
        self.assertEqual(self.root.find_by_uuid(uuid.uuid4()), None)

        self.assertEqual(self.branch2.find_by_uuid(self.branch2.uuid), self.branch2)
        self.assertEqual(self.branch2.find_by_uuid(self.leaf21.uuid), self.leaf21)
        self.assertEqual(self.branch2.find_by_uuid(self.leaf11.uuid), None)

    def test_find_by_name(self):
        self.assertEqual(self.root.find_by_name(self.root.name), self.root)
        self.assertEqual(self.root.find_by_name(self.branch1.name), self.branch1)
        self.assertEqual(self.root.find_by_name(self.leaf11.name), self.leaf11)
        self.assertEqual(self.root.find_by_name("kamoulox"), None)

        self.assertEqual(self.branch2.find_by_name(self.branch2.name), self.branch2)
        self.assertEqual(self.branch2.find_by_name(self.leaf21.name), self.leaf21)
        self.assertEqual(self.branch2.find_by_name(self.leaf11.name), None)

    def test_add_child(self):
        parent = Object3D()
        child = Object3D()
        parent.add_child(child)
        self.assertEqual(parent.children, [child])
        self.assertEqual(child.parent, parent)

    def test_add_child_with_scene(self):
        obj = Object3D()
        with mock.patch.object(obj, 'added_to_parent', wraps=obj.added_to_parent) as added_parent, \
                mock.patch.object(obj, 'added_to_model') as added_model, \
                mock.patch.object(obj, 'added_to_scene') as added_scene:
            child = self.root.add_child(obj)
            self.assertEqual(child, obj)
            self.assertEqual(self.root.children, [self.branch1, self.branch2, obj])
            added_parent.assert_called_once_with()
            added_model.assert_called_once_with(False)
            added_scene.assert_called_once_with()

    def test_add_child_without_scene(self):
        obj = Object3D()
        with mock.patch.object(obj, 'added_to_parent', wraps=obj.added_to_parent) as added_parent, \
                mock.patch.object(obj, 'added_to_model') as added_model, \
                mock.patch.object(obj, 'added_to_scene') as added_scene:
            child = self.detached.add_child(obj)
            self.assertEqual(child, obj)
            self.assertEqual(self.detached.children, [obj])
            added_parent.assert_called_once_with()
            added_model.assert_not_called()
            added_scene.assert_not_called()

    def test_add_child_inherit_model_and_scene(self):
        obj = Object3D()
        self.root.add_child(obj)
        self.assertEqual(obj.model, self.root.model)
        self.assertEqual(obj.scene, self.root.scene)

    def test_remove_child(self):
        with mock.patch.object(self.leaf11, 'removed_from_parent', wraps=self.leaf11.removed_from_parent) as removed_parent, \
                mock.patch.object(self.leaf11, 'removed_from_model', wraps=self.leaf11.removed_from_model) as removed_model, \
                mock.patch.object(self.leaf11, 'removed_from_scene', wraps=self.leaf11.removed_from_scene) as removed_scene:
            child = self.branch1.remove_child(self.leaf11)
            self.assertEqual(child, self.leaf11)
            self.assertEqual(self.branch1.children, [self.leaf12, self.leaf13])
            removed_parent.assert_called_once_with()
            removed_model.assert_called_once_with(False)
            removed_scene.assert_called_once_with()

    def test_remove_child_clears_model_and_scene(self):
        self.branch1.remove_child(self.leaf11)
        self.assertIsNone(self.leaf11.model)
        self.assertIsNone(self.leaf11.scene)

    def test_remove_child_not_in_parent(self):
        with mock.patch.object(self.leaf11, 'removed_from_parent') as removed:
            child = self.branch2.remove_child(self.leaf11)
            self.assertIsNone(child)
            self.assertEqual(self.branch2.children, [self.leaf21, self.leaf22, self.leaf23])
            removed.assert_not_called()

    def test_remove_all_children(self):
        with mock.patch.object(self.leaf11, 'removed_from_parent') as removed1, \
                mock.patch.object(self.leaf12, 'removed_from_parent') as removed2, \
                mock.patch.object(self.leaf13, 'removed_from_parent') as removed3:
            self.branch1.remove_all_children()
            self.assertEqual(self.branch1.children, [])
            removed1.assert_called_once_with()
            removed2.assert_called_once_with()
            removed3.assert_called_once_with()

    def test_set_parent(self):
        parent = Object3D()
        child = Object3D()
        with mock.patch.object(child, "removed_from_parent") as removed, \
                mock.patch.object(child, "added_to_parent") as added:
            child.parent = parent
            self.assertEqual(child.parent, parent)
            removed.assert_not_called()
            added.assert_called_once_with()

    def test_switch_parent(self):
        parent1 = Object3D()
        parent2 = Object3D()
        child = Object3D()
        child.parent = parent1
        with mock.patch.object(child, "removed_from_parent") as removed, \
                mock.patch.object(child, "added_to_parent") as added:
            child.parent = parent2
            self.assertEqual(child.parent, parent2)
            removed.assert_called_once_with()
            added.assert_called_once_with()

    def test_no_switch_if_same_parent(self):
        parent = Object3D()
        child = Object3D()
        child.parent = parent
        with mock.patch.object(child, "removed_from_parent") as removed, \
                mock.patch.object(child, "added_to_parent") as added:
            child.parent = parent
            self.assertEqual(child.parent, parent)
            removed.assert_not_called()
            added.assert_not_called()

    def test_remove_parent(self):
        parent = Object3D()
        child = Object3D()
        child.parent = parent
        with mock.patch.object(child, "removed_from_parent") as removed, \
                mock.patch.object(child, "added_to_parent") as added:
            child.parent = None
            self.assertEqual(child.parent, None)
            removed.assert_called_once_with()
            added.assert_not_called()

    def test_setting_model_propagates_to_children(self):
        model = Model()
        parent = Object3D()
        child = Object3D()
        parent.add_child(child)
        parent.set_model(model)
        self.assertEqual(parent.model, model)
        self.assertEqual(child.model, model)

    def test_setting_model_propagates_to_behaviors(self):
        model = Model()
        obj = Object3D()
        behavior = Behavior()
        obj.add_behavior(behavior)
        obj.set_model(model)
        self.assertEqual(behavior.owners[model], 1)

    def test_set_scene(self):
        scene = Scene()
        child = Object3D()
        with mock.patch.object(child, "removed_from_scene") as removed, \
                mock.patch.object(child, "added_to_scene") as added:
            child.scene = scene
            self.assertEqual(child.scene, scene)
            removed.assert_not_called()
            added.assert_called_once_with()

    def test_switch_scene(self):
        scene1 = Scene()
        scene2 = Scene()
        child = Object3D()
        child.scene = scene1
        with mock.patch.object(child, "removed_from_scene") as removed, \
                mock.patch.object(child, "added_to_scene") as added:
            child.scene = scene2
            self.assertEqual(child.scene, scene2)
            removed.assert_called_once_with()
            added.assert_called_once_with()

    def test_no_switch_if_same_scene(self):
        scene = Scene()
        child = Object3D()
        child.scene = scene
        with mock.patch.object(child, "removed_from_scene") as removed, \
                mock.patch.object(child, "added_to_scene") as added:
            child.scene = scene
            self.assertEqual(child.scene, scene)
            removed.assert_not_called()
            added.assert_not_called()

    def test_remove_scene(self):
        scene = Object3D()
        child = Object3D()
        child.scene = scene
        with mock.patch.object(child, "removed_from_scene") as removed, \
                mock.patch.object(child, "added_to_scene") as added:
            child.scene = None
            self.assertEqual(child.scene, None)
            removed.assert_called_once_with()
            added.assert_not_called()

    def test_setting_scene_propagates_to_children(self):
        scene = Scene()
        parent = Object3D()
        child = Object3D()
        parent.add_child(child)
        parent.scene = scene
        self.assertEqual(parent.scene, scene)
        self.assertEqual(child.scene, scene)

    def test_set_scene_1(self):
        scene = Scene()
        self.root.scene = scene
        self.assertEqual(self.root.scene, scene)
        self.assertEqual(self.branch1.scene, scene)
        self.assertEqual(self.leaf11.scene, scene)
        self.assertEqual(self.leaf12.scene, scene)
        self.assertEqual(self.leaf13.scene, scene)
        self.assertEqual(self.branch2.scene, scene)
        self.assertEqual(self.leaf21.scene, scene)
        self.assertEqual(self.leaf22.scene, scene)
        self.assertEqual(self.leaf23.scene, scene)

    def test_set_scene_2(self):
        scene = Scene()
        scene.add_child(self.root)
        self.assertEqual(self.root.scene, scene)
        self.assertEqual(self.branch1.scene, scene)
        self.assertEqual(self.leaf11.scene, scene)
        self.assertEqual(self.leaf12.scene, scene)
        self.assertEqual(self.leaf13.scene, scene)
        self.assertEqual(self.branch2.scene, scene)
        self.assertEqual(self.leaf21.scene, scene)
        self.assertEqual(self.leaf22.scene, scene)
        self.assertEqual(self.leaf23.scene, scene)

    def test_gui_1(self):
        self.branch1.gui = True
        self.assertEqual(self.leaf11.gui, self.branch1.gui)
        self.assertEqual(self.leaf12.gui, self.branch1.gui)
        self.assertEqual(self.leaf12.gui, self.branch1.gui)
        self.assertNotEqual(self.leaf21.gui, self.branch1.gui)
        self.assertNotEqual(self.leaf22.gui, self.branch1.gui)
        self.assertNotEqual(self.leaf23.gui, self.branch1.gui)

    def test_gui_2(self):
        self.branch1.gui = True
        self.root.remove_child(self.branch1)
        self.branch2.add_child(self.branch1)
        self.assertEqual(self.branch1.gui, self.branch2.gui)
        self.assertEqual(self.leaf11.gui, self.branch2.gui)
        self.assertEqual(self.leaf12.gui, self.branch2.gui)
        self.assertEqual(self.leaf13.gui, self.branch2.gui)

    def test_registers_with_model_when_added_to_one(self):
        obj = Object3D()
        with mock.patch.object(self.scene.model, 'add_object') as add_obj:
            self.scene.add_child(obj)
            add_obj.assert_called_once_with(obj, False)

    def test_location(self):
        # Verify that I am getting a copy of the attribute location
        self.assertTrue(self.leaf11.location is not self.leaf11._location)

        # test the setter
        a_location = Vector3([1.0, 2.0, 3.0])
        self.leaf11.location = a_location
        self.assertEqual(self.leaf11.location, a_location)
        self.assertTrue(self.leaf11._location is not a_location)
        self.assertTrue(self.leaf11._dirty_matrix)

        # Because we are trying to set the location to the same location
        # the value should not be changed and dirty_matrix will remain false
        self.leaf11._dirty_matrix = False
        self.leaf11.location = a_location
        self.assertFalse(self.leaf11._dirty_matrix)

    def test_rotation(self):
        # Should be getting a copy of the attribute rotation
        self.assertTrue(self.leaf11.rotation is not self.leaf11._rotation)

        # test the setter
        a_rotation = Euler.from_x_rotation(math.pi)
        self.leaf11.rotation = a_rotation
        self.assertEqual(self.leaf11.rotation, a_rotation)
        self.assertTrue(self.leaf11._rotation is not a_rotation)
        self.assertTrue(self.leaf11._dirty_matrix)

        self.leaf11._dirty_matrix = False
        self.leaf11.rotation = a_rotation
        self.assertFalse(self.leaf11._dirty_matrix)

    def test_scale(self):
        self.assertTrue(self.leaf11.scale is not self.leaf11._scale)

        a_scale = Vector3([1.0, 2.0, 3.0])
        self.leaf11.scale = a_scale
        self.assertEqual(self.leaf11.scale, a_scale)
        self.assertTrue(self.leaf11._scale is not a_scale)
        self.assertTrue(self.leaf11._dirty_matrix)

        self.leaf11._dirty_matrix = False
        self.leaf11.scale = a_scale
        self.assertFalse(self.leaf11._dirty_matrix)

    def test_matrix_world(self):
        self.assertTrue(self.leaf11.matrix_world is not self.leaf11._matrix)

        leaf_mat = Matrix44.from_x_rotation(math.pi / 2.0)
        self.leaf11.matrix_world = leaf_mat
        self.assertTrue(self.leaf11._matrix is not leaf_mat)
        # at this point, all the parents of leaf have the identity matrix as world matrix
        self.assertEqual(self.leaf11.matrix_world, leaf_mat)

        branch_mat = Matrix44.from_y_rotation(math.pi / 2.0)
        self.branch1.matrix_world = branch_mat
        self.assertEqual(self.leaf11.matrix_world, branch_mat * leaf_mat)

    def test_location_offset(self):
        self.assertTrue(self.leaf11.location_offset is not self.leaf11._location_offset)

        a_location = Vector3([1.0, 2.0, 3.0])
        self.leaf11.location_offset = a_location
        self.assertEqual(self.leaf11.location_offset, a_location)
        self.assertTrue(self.leaf11._location_offset is not a_location)
        self.assertTrue(self.leaf11._dirty_matrix_offset)

        self.leaf11._dirty_matrix_offset = False
        self.leaf11.location_offset = a_location
        self.assertFalse(self.leaf11._dirty_matrix_offset)

    def test_rotation_offset(self):
        self.assertTrue(self.leaf11.rotation_offset is not self.leaf11._rotation_offset)

        a_rotation = Euler.from_x_rotation(math.pi)
        self.leaf11.rotation_offset = a_rotation
        self.assertEqual(self.leaf11.rotation_offset, a_rotation)
        self.assertTrue(self.leaf11._rotation_offset is not a_rotation)
        self.assertTrue(self.leaf11._dirty_matrix_offset)

        self.leaf11._dirty_matrix_offset = False
        self.leaf11.rotation_offset = a_rotation
        self.assertFalse(self.leaf11._dirty_matrix_offset)

    def test_scale_offset(self):
        self.assertTrue(self.leaf11.scale_offset is not self.leaf11._scale_offset)

        a_scale = Vector3([1.0, 2.0, 3.0])
        self.leaf11.scale_offset = a_scale
        self.assertEqual(self.leaf11.scale_offset, a_scale)
        self.assertTrue(self.leaf11._scale_offset is not a_scale)
        self.assertTrue(self.leaf11._dirty_matrix_offset)

        self.leaf11._dirty_matrix_offset = False
        self.leaf11.scale_offset = a_scale
        self.assertFalse(self.leaf11._dirty_matrix_offset)


