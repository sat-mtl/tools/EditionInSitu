from unittest import TestCase, mock

from eis.graph.material import Material
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from eis.graph.texture import Texture


class TestScene(TestCase):
    def test_create_from_model(self):
        leaf = Object3D(name="leaf")
        branch = Object3D(name="branch", children=[leaf])
        root = Object3D(name="root", children=[branch])

        meshes = [Mesh()]
        materials = [Material()]
        textures = [Texture()]

        model = Model(
            root=root,
            meshes=meshes,
            materials=materials,
            textures=textures
        )

        scene = Scene(model=model)

        self.assertEqual(scene.model, model)
        self.assertEqual(scene.model.root, root)
        self.assertEqual(root.scene, scene)
        self.assertEqual(branch.scene, scene)
        self.assertEqual(leaf.scene, scene)
        self.assertEqual(scene.model.meshes, meshes)
        self.assertEqual(scene.model.materials, materials)
        self.assertEqual(scene.model.textures, textures)

    def test_default_model(self):
        scene = Scene()
        self.assertIsNotNone(scene.model)
        self.assertIsInstance(scene.model, Model)
        self.assertEqual(scene, scene.model.root.scene)

    def test_delegate_from_init(self):
        engine = mock.Mock()
        engine.delegate = mock.Mock()
        scene = Scene(engine=engine)
        self.assertEqual(scene.model.delegate, engine.delegate)

    def test_delegate_from_engine_setter(self):
        engine = mock.Mock()
        engine.delegate = mock.Mock()
        scene = Scene()
        scene.engine = engine
        self.assertEqual(scene.model.delegate, engine.delegate)

    def add_child_redirects_to_root(self):
        root = Object3D()
        model = Model(root=root)
        scene = Scene(model=model)
        obj = Object3D()
        with mock.patch.object(root, "add_child") as add:
            scene.add_child(obj)
            add.assert_called_once_with(obj)

    def remove_child_redirects_to_root(self):
        root = Object3D()
        model = Model(root=root)
        scene = Scene(model=model)
        obj = Object3D()
        scene.add_child(obj)
        with mock.patch.object(root, "remove_child") as remove:
            scene.remove_child(obj)
            remove.assert_called_once_with(obj)
