from unittest import TestCase, mock

from eis.editor import Editor
from .mock.editor import DummyEntity, DummySync, MockEditor


class TestEditor(TestCase):

    def test_property_changed(self):
        entity = DummyEntity()
        sync = DummySync()
        editor = MockEditor()

        editor.property_changed(entity, sync)
        editor.property_changed(entity, sync)  # Twice for good measure
        self.assertEqual({entity: {sync}}, editor._changed_properties)

        with mock.patch.object(editor.delegate, "on_property_changed") as opc:
            editor._sync_properties()
            opc.assert_called_once_with(entity=entity, sync=sync)

        # Should be cleared now that it was synced
        self.assertEqual({}, editor._changed_properties)

    def test_scene_delegate_change(self):
        editor = MockEditor()

        self.assertIsNone(editor.scene.model.delegate)

        delegate = mock.Mock()
        editor.delegate = delegate

        self.assertEqual(delegate, editor.scene.model.delegate)
