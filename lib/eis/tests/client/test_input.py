from unittest import TestCase

from eis.client.input import LocalInputMethod
from eis.engines.dummy.picker import DummyPicker
from ..mock.editor import DummyEngine, MockEditor
from satmath.quaternion import Quaternion


class TestLocalInputMethod(TestCase):
    def setUp(self):
        picker = DummyPicker(engine=DummyEngine(), editor=MockEditor())
        self.input_method = LocalInputMethod(config={}, mapping_config={}, picker=picker)

    def test_primary_orientation_copy(self):
        self.assertTrue(self.input_method.primary_orientation is not self.input_method._primary_orientation)
        self.assertEqual(type(self.input_method.primary_orientation), Quaternion)

    def test_secondary_orientation_copy(self):
        self.assertTrue(self.input_method.secondary_orientation is not self.input_method._secondary_orientation)
        self.assertEqual(type(self.input_method.secondary_orientation), Quaternion)

