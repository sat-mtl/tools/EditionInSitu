from unittest import TestCase

from eis.message import EISMessageParser, VERSION
from satnet.errors import ParsingError
from satnet.message import MessageDataType, Message
from .mock.serializable import DummySerializable


class TestMessage(TestCase):
    def test_message(self):
        s = DummySerializable()
        m = Message(s, b'RAW', b'CLIENT')
        self.assertEqual(m.data, s)
        self.assertEqual(m.raw, b'RAW')
        self.assertEqual(m.session, b'CLIENT')

    def test_empty_serialization(self):
        with self.assertRaisesRegex(ParsingError, 'Nothing to encode'):
            EISMessageParser.to_bytes(Message())

    def test_raw_serialization(self):
        m = Message(raw=b'RAW')
        self.assertEqual(EISMessageParser.to_bytes(m), b'EIS' + bytes([VERSION, MessageDataType.RAW.value]) + b'RAW')

    def test_data_serialization(self):
        s = DummySerializable()
        m = Message(data=s)
        self.assertEqual(EISMessageParser.to_bytes(m),
                         b'EIS' + bytes([VERSION, MessageDataType.SERIALIZED.value]) + s.serialize())

    def test_empty_deserialization(self):
        with self.assertRaisesRegex(ParsingError, 'Message too short'):
            EISMessageParser.from_bytes(b'EIS' + bytes([VERSION]))

    def test_raw_deserialization(self):
        m = EISMessageParser.from_bytes(b'EIS' + bytes([VERSION, MessageDataType.RAW.value]) + b'RAW')
        self.assertIsNotNone(m)
        self.assertEqual(m.raw, b'RAW')
        self.assertIsNone(m.data)
        self.assertIsNone(m.session)

    def test_data_deserialization(self):
        s = DummySerializable()
        s.name = "Pouet"
        m = EISMessageParser.from_bytes(b'EIS' + bytes([VERSION, MessageDataType.SERIALIZED.value]) + s.serialize())
        self.assertIsNotNone(m)
        self.assertEqual(m.raw, s.serialize())
        self.assertIsInstance(m.data, DummySerializable)
        self.assertEqual(m.data.name, s.name)
        self.assertIsNone(m.session)

    def test_too_short(self):
        with self.assertRaisesRegex(ParsingError, 'Message too short'):
            EISMessageParser.from_bytes(b'EIS')

    def test_wrong_protocol(self):
        with self.assertRaisesRegex(ParsingError, 'Wrong protocol'):
            EISMessageParser.from_bytes(b'THIS IS LONG ENOUGH BUT IS NOT AN EIS MESSAGE')

    def test_invalid_data_type(self):
        # If 0xFF ever become valid we'll need to change this test
        with self.assertRaisesRegex(ParsingError, 'Invalid data type*'):
            EISMessageParser.from_bytes(b'EIS' + bytes([VERSION, 0xFF]))

    def test_unsupported_version(self):
        with self.assertRaisesRegex(ParsingError, 'Unsupported protocol version*'):
            EISMessageParser.from_bytes(b'EIS' + bytes([0xFF, 0x00]))
