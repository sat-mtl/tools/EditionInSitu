from enum import IntEnum, unique
from typing import TYPE_CHECKING

from satnet.command import Command, ClientCommand, ServerCommand

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.editor import Editor
    # noinspection PyUnresolvedReferences
    from eis.session import EISSession
    # noinspection PyUnresolvedReferences
    from eis.client.session import EISLocalSession
    # noinspection PyUnresolvedReferences
    from eis.server.session import EISRemoteSession


@unique
class CommandId(IntEnum):
    """
    EIS Command Ids
    """
    READY = 0x01
    ADD_USER = 0x02
    REMOVE_USER = 0x03

    POSITION_CHANGED = 0x10
    MOVE = 0x11
    TRANSFORM = 0x12
    TRANSFORM_OFFSET = 0x13

    PROPERTY_CHANGED = 0x20
    ADD_MODEL = 0x21
    ADD_OBJECT3D = 0x22
    REMOVE_OBJECT3D = 0x23
    ADD_ANIMATION_CURVE = 0x24
    REMOVE_ANIMATION_CURVE = 0x25
    ADD_BEHAVIOR = 0x26
    REMOVE_BEHAVIOR = 0x27
    REMOVE_BEHAVIOR_BY_TYPE = 0x28
    ADD_MATERIAL = 0x29
    REMOVE_MATERIAL = 0x2a
    ADD_TEXTURE = 0x2b
    REMOVE_TEXTURE = 0x2C
    ADD_MESH = 0x2D
    REMOVE_MESH = 0x2E
    ADD_EVENT_HANDLER = 0x2F
    REMOVE_EVENT_HANDLER = 0x30

    SAVE_SCENE_FILE = 0x40
    LOAD_SCENE_FILE = 0x41
    LOAD_SCENE = 0x42

    START_TIMELINE = 0x50
    STOP_TIMELINE = 0x51
    RESET_TIMELINE = 0x52
    NEXT_KEYFRAME = 0x53
    PREVIOUS_KEYFRAME = 0x54

    TEST = 0xff


class EISCommand(Command['EISSession[Editor]']):
    """
    Client command, can only be used by clients
    Helper wrapper to save on imports and generic type declarations in concrete eis commands
    """
    pass


class EISClientCommand(ClientCommand['EISRemoteSession']):
    """
    Client command, can only be used by clients
    Helper wrapper to save on imports and generic type declarations in concrete eis commands
    """
    pass


class EISServerCommand(ServerCommand['EISLocalSession']):
    """
    Server command, can only be used by servers
    Helper wrapper to save on imports and generic type declarations in concrete eis commands
    """
    pass
