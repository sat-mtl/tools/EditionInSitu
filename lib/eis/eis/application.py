import json
import logging
import os
import signal
from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Optional
from eis import BASE_PATH, EIS_CONFIG_FILENAME, LOCAL_CONFIG_FILENAME
from profiling.sampling import SamplingProfiler  # type: ignore

from satlib.utils.config_utils import load_json_config, update_json_config
from satnet.base import defaultNetworkConfig

logger = logging.getLogger(__name__)

defaultNetworkConfig['host'] = "localhost"
defaultNetworkConfig['port'] = 9000


class EISApplication(metaclass=ABCMeta):
    """
    Base class for an EIS application
    """

    config_path = os.path.join(BASE_PATH, "config")

    def __init__(
            self,
            repl: bool = False,
            app_config: Optional[str] = None,
            override_config: Optional[str] = None,
            profile: bool = False
    ) -> None:
        """
        Initialize an EIS application

        :param repl: bool - Start in REPL mode
        :param app_config: Optional[str] - Application config file
        :param override_config: Optional[str] - Override config file (passed by command-line, for example)
        """

        self._continue_running = True

        # Import all serializable classes
        # Done here so as not to cause initialization order issues
        from .all_serializable import import_all
        import_all()

        self._repl = repl
        self._repl_dict: Dict[str, Any] = {}

        # Profiling
        self._profile = profile

        # Read the config files
        self._config: Dict[str, Any] = dict()
        self._config_local: Dict[str, Any] = dict()

        config_path = os.path.join(EISApplication.config_path, EIS_CONFIG_FILENAME)
        self._config = load_json_config(config_path)

        if app_config is not None:
            update_json_config(self._config, app_config)

        local_config_path = os.path.join(EISApplication.config_path, LOCAL_CONFIG_FILENAME)
        update_json_config(self._config, local_config_path)
        update_json_config(self._config_local, local_config_path)

        if override_config is not None:
            update_json_config(self._config, override_config)

    def handle_signals(self, signum, stack) -> None:
        self._continue_running = False
        print("Exiting...")

    def run(self) -> None:
        """
        Run the app
        This should contain a main loop and never return

        :return: None
        """

        # Start the REPL
        if self._repl:
            import code
            import readline
            import rlcompleter
            from threading import Thread

            readline.parse_and_bind("tab: complete")
            readline.set_completer(rlcompleter.Completer(namespace=self._repl_dict).complete)  # type: ignore

            def repl_thread() -> None:
                console = code.InteractiveConsole(locals=self._repl_dict)
                console.interact()

            Thread(target=repl_thread, name="REPL").start()

        # Start profiling
        if self._profile:
            profiler = SamplingProfiler()
            profiler.start()

        while self._continue_running:
            signal.signal(signal.SIGINT, self.handle_signals)
            signal.signal(signal.SIGTERM, self.handle_signals)
            if self._profile:
                try:
                    self.step()
                except KeyboardInterrupt:
                    self._continue_running = False
            else:
                self.step()

        # Get profiling results
        if self._profile:
            profiler.stop()
            profiler.run_viewer()

    @abstractmethod
    def step(self) -> None:
        pass
