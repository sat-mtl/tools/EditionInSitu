import logging
from typing import Any, Dict, List

from eis.assets.satie_asset import SatieAsset as Asset
from eis.editor_support.asset_manager import AssetManager
from eis.editor_support.satie_manager import SatieManager


logger = logging.getLogger(__name__)


class SatieAssetManager(AssetManager):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._satie_manager = SatieManager(config=self._library.editor.config)
        self._changed_plugins: List[Dict[str, Any]] = []
        self._satie_plugins: List[Any] = []
        # get SATIE resources
        self._satie_manager.satie.audiosources()
        Asset.init_plugins()

    @property
    def satie_manager(self):
        return self._satie_manager

    @property
    def satie_plugins(self) -> List[str]:
        return self._satie_plugins

    @satie_plugins.setter
    def satie_plugins(self, items: List[str]) -> None:
        self._satie_plugins = items

    def step(self, now: float, dt: float) -> None:
        if not self._active:
            return

        changed_plugins = list(self._changed_plugins)

        # Step in assets
        for asset in self._library._assets.values():
            asset.step(now=now, dt=dt)

        for f in changed_plugins:
            event = f['event']
            plugin = f['plugin']

            if event == 'modified':
                try:
                    asset = Asset.factory_create_asset(plugin)
                    if not asset:
                        logger.warning("Could not find a SATIE asset matching {}".format(plugin))
                        continue
                except Exception as e:
                    logger.warning("{} Could not create SATIE asset {}".format(e, plugin))
                else:
                    self._library.instantiate(asset)

        with self._lock:
            for f in changed_plugins:
                self._changed_plugins.remove(f)

    def activate(self) -> None:
        self._satie_manager.satie.audiosources()
        self._satie_plugins = self._satie_manager.satie.plugins
        for p in self._satie_plugins:
            self._changed_plugins.append({'event': 'modified', 'plugin': p})
        self._active = True

    def deactivate(self) -> None:
        self._changed_plugins = []
        self._active = False
