from typing import Any
from abc import abstractmethod
from eis.entity import Sync
from eis.graph.object_3d import Object3D


class AxisHelper(Object3D):

    def static_update_axis(self, sync: Sync, previous_value: Any, value: Any) -> None:
        self.update_axis(sync, value)

    radius = Sync[float]('_radius', 1.0, on_changed=static_update_axis)

    def __init__(
            self,
            *args: Any,
            radius: float = 1.0,
            **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self._radius = radius

    @abstractmethod
    def update_axis(self, sync: Sync, value: Any) -> None:
        raise NotImplementedError()
