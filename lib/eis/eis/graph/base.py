import logging
from abc import abstractmethod
from typing import Any, Dict, Generic, List, Optional, TYPE_CHECKING, Type, TypeVar, cast

from eis.entity import Sync, SyncEntity

if TYPE_CHECKING:
    from eis.engine import Engine
    from eis.graph.model import Model

logger = logging.getLogger(__name__)

O = TypeVar('O', bound='GraphBase')


class GraphProxyBase(Generic[O]):
    """
    Base class for graph proxies.
    Graph proxies of every possible EIS graph types must be implemented per engine
    in order to convert EIS objects to/from the engine specific objects/calls.
    """

    def __init__(self, engine: 'Engine', proxied: O) -> None:
        self._engine = engine
        self._proxied = proxied
        self._initialized = False
        self._valid = False

    @property
    def engine(self) -> 'Engine':
        return self._engine

    @property
    def proxied(self) -> O:
        return self._proxied

    def make(self) -> None:
        """
        Construct a target object, if a target is not passed in the constructor
        then this method is called to obtain an instance.
        :return: None
        """
        self._initialized = True
        self._valid = True  # It should be valid after creation

    def update_graph(self) -> None:
        """
        This is called if the EiS graph has been modified
        Takes care of updating the proxy graph accordingly
        """
        pass

    def update(self) -> None:
        self._valid = True


P = TypeVar('P', bound=GraphProxyBase[Any])
C = TypeVar('C', bound='GraphBase')


class GraphBase(Generic[P], SyncEntity):
    _fields: List[str] = ['_name']  # type: ignore

    def __init__(self, name: Optional[str] = None) -> None:
        super().__init__()
        self._managed = False
        self._name = name or ""
        self._proxy: Optional[P] = None
        self._dirty = True

    def __str__(self) -> str:
        return "{}\033[0;0m({}, \033[1m{}\033[0;0m) \033[33;1m->\033[0;0m Proxy {}".format(self.__class__.__name__, self.name, self.uuid, type(self.proxy) if self.proxy else None)

    @property
    def dirty(self) -> bool:
        return self._dirty

    @property
    def managed(self) -> bool:
        """
        Returns whether this graph entity is managed by application code.
        Graph entities that should not be serialized for saving/transferring must
        set managed to True so that it will only exist in the local engine.
        i.e.: A sphere-generating primitive should not save/transfer its mesh/geometry
              only the parameters are necessary.
        :return: bool
        """
        return self._managed

    @managed.setter
    def managed(self, value: bool) -> None:
        if self._managed != value:
            self._managed = value
            # Don't serialize managed objects
            self._serialize = not self._managed

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str) -> None:
        if self._name != value:
            self._name = value

    @property
    def proxy(self) -> Optional[P]:
        return self._proxy

    def update_proxy(self, engine: 'Engine') -> None:
        """
        Generate a proxy for this graph entity
        :param engine: Engine - Engine for which to generate the proxy
        :return: None
        """

        if not self._proxy:
            # NOTE: We went overboard with the generics and MyPy can't figure out that P is also GraphProxyBase[Any]
            self._proxy = engine.get_proxy(self)  # type: ignore
            if self._proxy:
                self._proxy.make()
            else:
                logger.error("Could not make proxy for {}".format(self))

        # NOTE: This used to only be a call to `proxy.update()` but since it wasn't
        #       validating the object3d, the next `step` would run `proxy.update` a second time
        #       doing it like this only updates once when added to the scene.
        self.update()

    def step(self, now: float, dt: float) -> None:
        if not self._proxy or self._dirty:
            self.update()

    def sync_invalidate(self, sync: Sync, previous_value: Any, value: Any) -> None:
        self.invalidate()

    def invalidate(self) -> None:
        """
        Invalidate the graph proxy
        :return: None
        """
        self._dirty = True

    def invalidate_graph(self) -> None:
        """
        Invalidate the graph. Especially useful for updating the proxy
        graph after a modification of the EiS graph
        :return: None
        """
        if self._proxy:
            self._proxy.update_graph()

    def update(self) -> None:
        """
        Update the graph proxy
        :return: None
        """
        if self._proxy:
            self._proxy.update()
        self._dirty = False

    def copy(self: C) -> C:
        return self._copy(type(self))

    def _copy(self: C, graph_type: Type['GraphBase'], *args: Any, **kwargs: Any) -> C:
        """
        Deep copy
        :return: GraphBase
        """
        kwargs["name"] = self._name
        return cast(C, graph_type(*args, **kwargs))

    def copy_shared(self: C) -> C:
        return self._copy_shared(type(self))

    @abstractmethod
    def _copy_shared(self: C, graph_type: Type['GraphBase'], *args: Any, **kwargs: Any) -> C:
        """
        Deep copy, preserving shared entities
        :return: GraphBase
        """
        kwargs["name"] = self._name
        return cast(C, graph_type(*args, **kwargs))


class UniqueGraphEntity(Generic[P], GraphBase[P]):

    def __init__(self, name: Optional[str] = None) -> None:
        super().__init__(name=name)
        self._model: Optional['Model'] = None

    @property
    def model(self) -> Optional['Model']:
        return self._model

    def set_model(self, value: Optional['Model'], silent: bool = False) -> None:
        if self._model != value:
            if self._model:
                self.removed_from_model(silent)
            self._model = value
            if self._model:
                self.added_to_model(silent)

    def added_to_model(self, silent: bool = False) -> None:
        """
        This is called, usually by a parent, to tell the entity that it was added to a rooted tree
        (or the branch is was attached to is now rooted). Since this is a unique entity it can
        only be under one tree so use `self._model` to access the model.

        :param silent: bool - Whether the model's delegate should be notified
        :return: None
        """
        pass

    def removed_from_model(self, silent: bool = False) -> None:
        """
        This is called, usually by a parent, to tell the entity that it was removed from a rooted tree
        (or the branch on which it is was severed from the tree). Since this is a unique entity it can
        only be under one tree so use `self._model` to access the model (last chance before it becomes None).

        :param silent: bool - Whether the model's delegate should be notified
        :return: None
        """
        pass


class SharedGraphEntity(Generic[P], GraphBase[P]):

    def __init__(self, name: Optional[str] = None) -> None:
        super().__init__(name=name)
        self._owners: Dict['Model', int] = {}

    @property
    def owners(self) -> Dict['Model', int]:
        return self._owners

    def add_owner(self, owner: 'Model') -> bool:
        """
        This is called each time this entity is added/shared.
        Unlike added_to_model which is called only the first time
        the entity finds itself under a rooted tree, this is called
        every time the entity is shared in the tree, allowing for
        reference counting of the entity.
        :param owner: Model
        :return: True if the model is added for the first time
        """
        assert owner
        if owner not in self._owners:
            self._owners[owner] = 1
        else:
            self._owners[owner] += 1

        return self._owners[owner] == 1

    def remove_owner(self, owner: 'Model') -> bool:
        """
        This is called each time this entity is removed/unshared.
        Unlike removed_from_model which is called only when the entity
        leaves a rooted tree, this is called every time the entity is
        unshared in the tree, allowing for reference counting of the entity.
        :param owner: Model
        :return: True if the object was not used anymore in the whole Model, and thus has been removed
        """
        assert owner
        if owner in self._owners:
            assert self._owners[owner] > 0
            self._owners[owner] -= 1
            if self._owners[owner] <= 0:
                del self._owners[owner]
                return True
        return False

    def added_to_model(self, model: 'Model') -> None:
        """
        This is called, usually by a parent, to tell the entity added to a rooted tree
        (or the branch it was attached to is now rooted) under a new model.

        :param model: Model
        :return: None
        """
        pass

    def removed_from_model(self, model: 'Model') -> None:
        """
        This is called, usually by a parent, to tell the entity that it was removed
        from a rooted tree (or the branch on which it is was severed from the tree).
        It doesn't mean that it is not used anymore by another tree though.

        :param model: Model
        :return: None
        """
        pass

    ...
