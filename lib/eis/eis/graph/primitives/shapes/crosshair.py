from typing import Any, Type

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.primitives.shape import Shape
from satnet.entity import entity


@entity(id=EISEntityId.SHAPE_CROSSHAIR)
class Crosshair(Shape):
    # Don't sync points, we manage them internally
    Shape.points.enabled = False

    size = Sync[float]('_size', 1.0, on_changed=Shape.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            size: float = 1.0,
            **kwargs: Any

    ) -> None:
        super().__init__(*args, **kwargs)

        self._size = size

    def update(self) -> None:
        hs = self._size / 2

        # Because we don't have a curves array on our shape object
        # here we simply just pass by the center between the two lines
        self.points = [
            (0, hs, 0),
            (0, -hs, 0),
            (0, 0, 0),
            (-hs, 0, 0),
            (hs, 0, 0)
        ]

        super().update()

    def _copy(self, graph_type: Type['Crosshair'], *args: Any, **kwargs: Any) -> 'Crosshair':
        return super()._copy(
            graph_type,
            *args,
            size=self._size,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Crosshair'], *args: Any, **kwargs: Any) -> 'Crosshair':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius,
            **kwargs
        )
