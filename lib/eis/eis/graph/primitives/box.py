from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.material import Material
from eis.graph.primitives import Primitive, PrimitiveSignature
from satmath.matrix44 import Matrix44
from satnet.entity import entity


@entity(id=EISEntityId.PRIMITIVE_BOX)
class Box(Primitive):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/BoxGeometry.js
    """

    width = Sync[float]('_width', 1.0, on_changed=Primitive.sync_invalidate)
    height = Sync[float]('_height', 1.0, on_changed=Primitive.sync_invalidate)
    depth = Sync[float]('_depth', 1.0, on_changed=Primitive.sync_invalidate)
    width_segments = Sync[int]('_width_segments', 1, on_changed=Primitive.sync_invalidate)
    height_segments = Sync[int]('_height_segments', 1, on_changed=Primitive.sync_invalidate)
    depth_segments = Sync[int]('_depth_segments', 1, on_changed=Primitive.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            matrix: Optional[Matrix44] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            width: float = 1.0,
            height: float = 1.0,
            depth: float = 1.0,
            width_segments: int = 1,
            height_segments: int = 1,
            depth_segments: int = 1,
            **kwargs
    ) -> None:
        parameters = {
            'width': width,
            'height': height,
            'depth': depth,
            'width_segments': width_segments,
            'height_segments': height_segments,
            'depth_segments': depth_segments
        }
        kwargs['signature'] = PrimitiveSignature(primitive_type=EISEntityId.PRIMITIVE_BOX, parameters=parameters)

        super().__init__(
            *args,
            name=name,
            matrix=matrix,
            material=material,
            material_id=material_id,
            **kwargs
        )

        self._width = width
        self._height = height
        self._depth = depth
        self._width_segments = width_segments
        self._height_segments = height_segments
        self._depth_segments = depth_segments

        self._vertex_count = 0

    def build(self) -> None:
        """
        Build each side of the box geometry
        :return: None
        """

        super().build()

        X = 0
        Y = 1
        Z = 2

        self._vertex_count = 0

        def _build_plane(u: int, v: int, w: int, udir: float, vdir: float, width: float, height: float, depth: float, gridX: int, gridY: int) -> None:
            """
            Build one side of the box

            :param u:
            :param v:
            :param w:
            :param udir:
            :param vdir:
            :param width:
            :param height:
            :param depth:
            :param gridX:
            :param gridY:
            :return: None
            """
            segment_width = width / gridX
            segment_height = height / gridY

            width_half = width / 2
            height_half = height / 2
            depth_half = depth / 2

            grid_x1 = gridX + 1
            grid_y1 = gridY + 1

            vertex_counter = 0

            # generate vertices, normals and uvs
            vector = [0.00, 0.00, 0.00]
            for iy in range(0, grid_y1):
                y = iy * segment_height - height_half
                for ix in range(0, grid_x1):
                    x = ix * segment_width - width_half

                    vector[u] = x * udir
                    vector[v] = y * vdir
                    vector[w] = depth_half
                    self._geometry.vertices.append(tuple(vector))

                    vector[u] = 0
                    vector[v] = 0
                    vector[w] = 1 if depth > 0 else -1
                    self._geometry.normals.append(tuple(vector))

                    self._geometry.texcoords.append((1.0 - ix / gridX, 1.0 - iy / gridY))
                    vertex_counter += 1

            # indices

            # 1. you need three indices to draw a single face
            # 2. a single segment consists of two faces
            # 3. so we need to generate six (2*3) indices per segment
            for iy in range(0, gridY):
                for ix in range(0, gridX):
                    a = self._vertex_count + ix + grid_x1 * iy
                    b = self._vertex_count + ix + grid_x1 * (iy + 1)
                    c = self._vertex_count + (ix + 1) + grid_x1 * (iy + 1)
                    d = self._vertex_count + (ix + 1) + grid_x1 * iy

                    # faces
                    self._geometry.primitives.extend([[a, b, d], [b, c, d]])

            # update total number of vertices
            self._vertex_count += vertex_counter

        _build_plane(Z, Y, X, -1.0, -1.0, self._depth, self._height, self._width,
                     self._depth_segments, self._height_segments)  # px
        _build_plane(Z, Y, X, 1.0, -1.0, self._depth, self._height, -self._width,
                     self._depth_segments, self._height_segments)  # nx
        _build_plane(X, Z, Y, 1.0, 1.0, self._width, self._depth, self._height,
                     self._width_segments, self._depth_segments)  # py
        _build_plane(X, Z, Y, 1.0, -1.0, self._width, self._depth, -self._height,
                     self._width_segments, self._depth_segments)  # ny
        _build_plane(X, Y, Z, 1.0, -1.0, self._width, self._height, self._depth,
                     self._width_segments, self._height_segments)  # pz
        _build_plane(X, Y, Z, -1.0, -1.0, self._width, self._height, -self._depth,
                     self._width_segments, self._height_segments)  # nz

    def _copy(self, graph_type: Type['Box'], *args: Any, **kwargs: Any) -> 'Box':
        return super()._copy(
            graph_type,
            *args,
            width=self._width,
            height=self._height,
            depth=self._depth,
            width_segments=self._width_segments,
            height_segments=self._height_segments,
            depth_segments=self._depth_segments,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Box'], *args: Any, **kwargs: Any) -> 'Box':
        return super()._copy_shared(
            graph_type,
            *args,
            width=self._width,
            height=self._height,
            depth=self._depth,
            width_segments=self._width_segments,
            height_segments=self._height_segments,
            depth_segments=self._depth_segments,
            **kwargs
        )
