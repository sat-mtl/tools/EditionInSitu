import math
from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.material import Material
from eis.graph.primitives import Primitive, PrimitiveSignature
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3
from satnet.entity import entity


@entity(id=EISEntityId.PRIMITIVE_SPHERE)
class Sphere(Primitive):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/SphereGeometry.js
    """

    radius = Sync[float]('_radius', 0.5, on_changed=Primitive.sync_invalidate)
    width_segments = Sync[int]('_width_segments', 32, on_changed=Primitive.sync_invalidate)
    height_segments = Sync[int]('_height_segments', 32, on_changed=Primitive.sync_invalidate)
    phi_start = Sync[float]('_phi_start', 0, on_changed=Primitive.sync_invalidate)
    phi_length = Sync[float]('_phi_length', math.pi * 2.0, on_changed=Primitive.sync_invalidate)
    theta_start = Sync[float]('_theta_start', 0.0, on_changed=Primitive.sync_invalidate)
    theta_length = Sync[float]('_theta_length', math.pi, on_changed=Primitive.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            matrix: Optional[Matrix44] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            radius: float = 0.5,
            width_segments: int = 32,
            height_segments: int = 32,
            phi_start: float = 0,
            phi_length: float = math.pi * 2.0,
            theta_start: float = 0,
            theta_length: float = math.pi,
            **kwargs
    ) -> None:
        parameters = {
            'radius': radius,
            'width_segments': width_segments,
            'height_segments': height_segments,
            'phi_start': phi_start,
            'phi_length': phi_length,
            'theta_start': theta_start,
            'theta_length': theta_length
        }
        kwargs['signature'] = PrimitiveSignature(primitive_type=EISEntityId.PRIMITIVE_SPHERE, parameters=parameters)
        super().__init__(
            *args,
            name=name,
            matrix=matrix,
            material=material,
            material_id=material_id,
            **kwargs
        )

        self._radius = radius
        self._width_segments = width_segments
        self._height_segments = height_segments
        self._phi_start = phi_start
        self._phi_length = phi_length
        self._theta_start = theta_start
        self._theta_length = theta_length

    def build(self) -> None:
        super().build()

        theta_end = self._theta_start + self._theta_length

        index = 0
        grid = []

        normal = Vector3((0.00, 0.00, 0.00))

        # generate vertices, normals and uvs

        for iy in range(0, self._height_segments + 1):
            vertices_row = []
            v = iy / self._height_segments
            for ix in range(0, self._width_segments + 1):
                u = ix / self._width_segments

                # vertex
                vertex = (
                    - self._radius * math.cos(self._phi_start + u * self._phi_length) *
                    math.sin(self._theta_start + v * self._theta_length),
                    self._radius * math.cos(self._theta_start + v * self._theta_length),
                    self._radius * math.sin(self._phi_start + u * self._phi_length) *
                    math.sin(self._theta_start + v * self._theta_length)
                )

                self._geometry.vertices.append(vertex)

                # normal
                normal.x = vertex[0]
                normal.y = vertex[1]
                normal.z = vertex[2]
                normal.normalize()
                self._geometry.normals.append((normal.x, normal.y, normal.z))

                # uv
                self._geometry.texcoords.append((u, 1.0 - v))

                vertices_row.append(index)
                index += 1

            grid.append(vertices_row)

        # indices
        for iy in range(0, self._height_segments):
            for ix in range(0, self._width_segments):
                a = grid[iy][ix + 1]
                b = grid[iy][ix]
                c = grid[iy + 1][ix]
                d = grid[iy + 1][ix + 1]

                if iy != 0 or self._theta_start > 0:
                    self._geometry.primitives.append([a, b, d])
                if iy != self._height_segments - 1 or theta_end < math.pi:
                    self._geometry.primitives.append([b, c, d])

    # region Lifecycle

    # endregion

    # region Copy

    def _copy(self, graph_type: Type['Sphere'], *args: Any, **kwargs: Any) -> 'Sphere':
        return super()._copy(
            graph_type,
            *args,
            radius=self._radius,
            width_segments=self._width_segments,
            height_segments=self._height_segments,
            phi_start=self._phi_start,
            phi_length=self._phi_length,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Sphere'], *args: Any, **kwargs: Any) -> 'Sphere':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius,
            width_segments=self._width_segments,
            height_segments=self._height_segments,
            phi_start=self._phi_start,
            phi_length=self._phi_length,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    # endregion
