import os
from typing import Any, Callable, Optional
from eis import EISEntityId
from eis.graph.camera import Camera
from eis.graph.object_3d import Object3D
from satnet.entity import entity


@entity(id=EISEntityId.CAMERA_PRESENTATION)
class Camera_Presentation(Camera):

    _camera_3d_model: Optional[Object3D] = None

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

    def initialize(self) -> None:
        super().initialize()

    def _load_model_and_add_to_scene(self) -> None:
        if Camera_Presentation._camera_3d_model is None:
            Camera_Presentation._camera_3d_model = self.load_model()
        if Camera_Presentation._camera_3d_model is not None:
            self.add_child(Camera_Presentation._camera_3d_model.copy())
        self._added_model_to_scene = True

    def load_model(self) -> Optional[Object3D]:
        path = os.path.join(os.path.realpath(".."), self._scene.editor.config.get("camera_presentation.model"))
        scene = self._scene.editor.load_scene_from_file(path=path)
        if scene is not None:
            return scene.model.root
        return None

    def set_as_editor_camera(self, is_editor: bool, callback: Optional[Callable]) -> None:
        """
        Set this camera so that the editor follows it. This should not be called manually, change
        the camera on the editor instead (this function will then be called by the editor)
        """
        super().set_as_editor_camera(is_editor=is_editor, callback=callback)
        self.pickable = False
