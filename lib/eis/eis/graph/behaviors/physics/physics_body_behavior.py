import logging
from typing import List, Optional

import pybullet as bullet
import math

from eis import EISEntityId
from eis.constants import CLIENT
from eis.graph.behavior import behavior
from eis.graph.behaviors.physics.physics_behavior import PhysicsBehavior
from eis.graph.object_3d import Object3D
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)


@behavior(id=EISEntityId.BEHAVIOR_BODY_PHYSICS)
class PhysicsBodyBehavior(PhysicsBehavior):
    """
    Behavior adding physics properties to an object that represents the editor's body. 
    The behavior will synchronize itself with the editor automatically.
    There should never be more than one PhysicsBodyBehavior under the same client.
    :param density: float - The higher the density, the higher the mass. If 0.0, the object is
    passive and will not move but will interact with other physics-enabled objects
    """

    _fields = ['_density']  # type: ignore

    def __init__(self, density: Optional[float] = None) -> None:
        super().__init__()
        self._max_velocity = 5.0
        self._density = 1.0
        self._lateral_friction = 0.9
        self._spinning_friction = 50.0
        self._rolling_friction = 50.0
        self._restitution = 0.4

    @property
    def max_velocity(self) -> float:
        return self._max_velocity

    @max_velocity.setter
    def max_velocity(self, value: float) -> None:
        self._max_velocity = value

    def step(self, now: float, dt: float) -> None:
        assert(isinstance(self._graphbase, Object3D))
        super(PhysicsBehavior, self).step(now=now, dt=dt)

        if CLIENT and self._bound_box is None:
            # We cannot do it in the added_to_object callback because when we load behaviors from a project file
            # the client's objects have not been attached to the scene yet and don't have a proxy, hence no bounding box yet
            self.compute_bound_box()

        if CLIENT and self._body != -1 and self._mass != 0.0:
            position = bullet.getBasePositionAndOrientation(bodyUniqueId=self._body)[0]
            matrix_world = self._graphbase.matrix_world
            scaled_position = Vector3(position) * matrix_world.scale
            matrix_world.translation = scaled_position
            self._graphbase.matrix_world = matrix_world

    def added_to_scene(self) -> None:
        super().added_to_scene()
        if CLIENT and self._graphbase is not None and self._scene is not None and self._scene.editor is not None:
            self._scene.editor.synchronized_physical_body = self._graphbase

    def removed_from_scene(self) -> None:
        super().removed_from_scene()
        if CLIENT and self._graphbase is not None and self._scene is not None and self._scene.editor is not None:
            self._scene.editor.synchronized_physical_body = None

    def reset_base_position(self, position: Optional[Vector3]) -> None:
        newposition = position if position is not None else Vector3((0, 0, 0))
        bullet.resetBasePositionAndOrientation(bodyUniqueId=self._body, posObj=[
                                               newposition[0], newposition[1], newposition[2]], ornObj=[0.0, 0.0, 0.0, 1.0])

    # used for navigating
    def increment_velocity(self, increment: List[float]) -> None:
        if CLIENT and self._body != -1:
            new_velocity = list(bullet.getBaseVelocity(bodyUniqueId=self._body)[0])
            for i in range(0, 2):
                new_velocity[i] += increment[i]

            length = math.sqrt(pow(new_velocity[0], 2) + pow(new_velocity[1], 2) + pow(new_velocity[2], 2))

            if length > self._max_velocity:
                for i in range(0, 2):
                    new_velocity[i] *= self._max_velocity / length

            bullet.resetBaseVelocity(objectUniqueId=self._body, linearVelocity=new_velocity)
