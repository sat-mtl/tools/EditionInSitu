import logging
from enum import IntEnum, unique
from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph import Color
from eis.graph.material import Material
from eis.graph.object_3d import Object3D
from satnet.entity import entity

logger = logging.getLogger(__name__)


@entity(id=EISEntityId.PRIMITIVE_TEXT)
class Text(Object3D):

    _fields = ['material_id']

    @unique
    class Align(IntEnum):
        CENTER = 0
        LEFT = 1
        RIGHT = 2

    def text_color_changed(self, sync: Sync, previous_value: Any, value: Any) -> None:
        if self._material:
            self._material.color = self._text_color

    text = Sync[str]('_text', "", on_changed=Object3D.sync_invalidate)
    text_color = Sync[Color]('_text_color', (0.0, 0.0, 0.0, 1.0), on_changed=text_color_changed)
    text_scale = Sync[float]('_text_scale', 1.0, on_changed=Object3D.sync_invalidate)
    align = Sync[Align]('_align', Align.LEFT, on_changed=Object3D.sync_invalidate)
    font = Sync[str]('_font', "", on_changed=Object3D.sync_invalidate)
    shadow_x = Sync[float]('_shadow_x', 0., on_changed=Object3D.sync_invalidate)
    shadow_y = Sync[float]('_shadow_x', 0., on_changed=Object3D.sync_invalidate)
    shadow_color = Sync[float]('_shadow_color', (0., 0., 0., 1.), on_changed=Object3D.sync_invalidate)

    def __init__(
            self,

            *args: Any,

            name: Optional[str] = None,

            text: str = "",
            text_scale: float = 1.0,
            align: Align = Align.LEFT,
            font: Optional[str] = None,
            shadow_x: float = 0.,
            shadow_y: float = 0.,
            shadow_color: Color = (0., 0., 0., 1.),

            text_color: Optional[Color] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            **kwargs: Any
    ) -> None:
        super().__init__(*args, name=name or text, **kwargs)

        self._material = material
        if not self._material:
            self._material = Material(
                color=text_color or (0.0, 0.0, 0.0, 1.0),
                refraction=0.0,
                roughness=1.0,
                metallic=0.0
            )
            self._material.managed = True
        elif text_color:
            self._material.color = text_color

        self._material_id = material.uuid if material is not None else material_id

        self._text = text
        self._text_color = self._material.color
        self._text_scale = text_scale
        self._align = align
        self._font = font
        self._shadow_x = shadow_x
        self._shadow_y = shadow_y
        self._shadow_color = shadow_color

    @property
    def material(self) -> Optional[Material]:
        return self._material

    @material.setter
    def material(self, value: Optional[Material]) -> None:
        if self._material != value:
            if self._material is not None:
                if self._model is not None:
                    self._material.removed_from_model(self._model)
            self._material = value
            if self._material is not None:
                if self._model is not None:
                    self._material.added_to_model(self._model)
            # Take id from material
            self._material_id = self._material.uuid if self._material else None
            self.invalidate_graph()

    @property
    def material_id(self) -> Optional[UUID]:
        return self._material_id

    def added_to_model(self, silent: bool = False) -> None:
        assert self._model
        super().added_to_model(silent)

        # Retrieve if we only hold the reference id
        if not self._material and self._material_id:
            # Retrieve the referenced instance, setter will take care of registration
            self.material = self._model.get_material_by_uuid(self._material_id)
            if not self._material:
                logger.warning("Could not retrieve material id \"{}\"".format(self._material_id))

        # Add to model
        if self._material is not None:
            self._material.added_to_model(self._model)

    def removed_from_model(self, silent: bool = False) -> None:
        assert self._model
        super().removed_from_model(silent)

        # Remove from model
        if self._material is not None:
            self._material.removed_from_model(self._model)

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        if self._material:
            self._material.step(now=now, dt=dt)

    def _copy(self, graph_type: Type['Text'], *args: Any, **kwargs: Any) -> 'Text':
        return super()._copy(
            graph_type,
            *args,
            material=self._material.copy() if self._material is not None and not self._material.managed else None,
            material_id=self._material_id if self._material is None or not self._material.managed else None,
            text=self._text,
            text_scale=self._text_scale,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Text'], *args: Any, **kwargs: Any) -> 'Text':
        return super()._copy_shared(
            graph_type,
            *args,
            # Materials are shared, so no copy, not even _shared
            material=self._material if self._material is not None and not self._material.managed else None,
            material_id=self._material_id if self._material is None or not self._material.managed else None,
            text=self._text,
            text_scale=self._text_scale,
            **kwargs
        )

    # endregion
    ...
