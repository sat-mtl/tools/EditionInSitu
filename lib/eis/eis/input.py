from abc import abstractmethod
from typing import Any, Generic, List, TYPE_CHECKING, TypeVar, Dict, Tuple, Optional, Union

from rx.subjects import Subject  # type: ignore

from satlib.categories import Disposable
from satnet.entity import Entity

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.user import EISUser

U = TypeVar('U', bound='EISUser[Any, Any, Any]')


class InputMethod(Entity, Disposable, Generic[U]):
    """
    EIS Input Method Abstract Base Class
    Extend this class and implement the abstract methods to create an input method.
    """

    def __init__(self, mapping_config=Dict[str, Any]) -> None:
        super().__init__()
        self._user: Optional[U] = None
        self._sync = False
        self._button_subject = Subject()
        self._analog_subject = Subject()
        self._tracker_subject = Subject()
        self._mouse_subject = Subject()

        # Common controls
        self._common_subject = Subject()

        self._mapping_dict: Dict[str, Optional[Tuple[str, List[Union[str, List[str]]]]]] = dict.fromkeys([
            'free_move_forward', 'free_move_backward', 'constrained_move_forward', 'constrained_move_backward',
            'constrained_move_left', 'constrained_move_right', 'constrained_move_up', 'constrained_move_down',
            'rotate', 'increase_speed', 'decrease_speed', 'select', 'hold', 'confirm', 'help', 'recall',
            'alternative_select', 'quick_translate', 'quick_rotate', 'quick_scale', 'menu', 'undo', 'reset',
            'slow_down', 'speed_up', 'navigate', 'redo', 'axis_x', 'axis_y', 'axis_z', 'axis_w', 'engage', 'hands',
            'drag', 'distance', 'toggle_timeline'
        ])
        self._mapping_config = mapping_config

    def _apply_config(self) -> None:
        if self._mapping_config is not None:
            for mapping_key in self._mapping_dict:
                mapping = self._mapping_dict[mapping_key]
                if mapping is not None:
                    for control in range(len(mapping[1])):
                        for config_key in self._mapping_config:
                            if mapping[1][control] == config_key:
                                mapping[1][control] = self._mapping_config[config_key]
                                break

    def dispose(self) -> None:
        self.user = None
        self._button_subject = None
        self._analog_subject = None
        self._tracker_subject = None
        self._mouse_subject = None
        self._common_subject = None
        super().dispose()

    @property
    def mapping_dict(self) -> dict:
        return self._mapping_dict

    @property
    def analog_subject(self) -> Subject:
        return self._analog_subject

    @property
    def button_subject(self) -> Subject:
        return self._button_subject

    @property
    def common_subject(self) -> Subject:
        return self._common_subject

    @property
    def tracker_subject(self) -> Subject:
        return self._tracker_subject

    @property
    def mouse_subject(self) -> Subject:
        return self._mouse_subject

    @property
    def sync(self) -> bool:
        """
        Syncing an input method allows the server and/or peers
        to preview the actions taken from this input method.
        For example: Sharing/syncing the Vive controllers allows
        the other user to see what the peer is doing with the controllers.
        :return: bool
        """
        return self._sync

    @property
    def user(self) -> Optional[U]:
        """
        User owning the input method
        :return: optional[U]
        """
        return self._user

    @user.setter
    def user(self, value: Optional[U]) -> None:
        if self._user != value:
            self._user = value

    @abstractmethod
    def step(self, now: float, dt: float) -> None:
        """
        Input step method

        :param now: float
        :param dt: float
        :return: None
        """
        pass

    def notify(self) -> None:
        """
        Notify the server/clients (depending if this is a local or remote
        implementation) of the input status.
        This is triggered by a task in the User class.
        :return: None
        """
        pass


class PeerInputMethod(InputMethod, Disposable):
    """
    Peer input method base
    This base class is used to represent an input method of a peer
    on the client-side. This is useful for things like syncing the
    HTC Vive Controller positions.
    """

    def __init__(self) -> None:
        super().__init__()
