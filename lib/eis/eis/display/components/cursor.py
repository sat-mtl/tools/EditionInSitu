import math
from typing import Any, Optional, TYPE_CHECKING

from eis.display.components.menus.menu import Menu
from eis.graph import Color
from eis.graph.material import Material
from eis.graph.object_3d import Object3D
from eis.graph.primitives.shape import Shape
from eis.graph.primitives.shapes.circle import Circle
from eis.graph.primitives.shapes.crosshair import Crosshair
from eis.graph.text import Text
from eis.utils.color import Colors
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.picker import Picker


class Chevron(Shape):
    # Don't sync points, we manage them internally
    Shape.points.enabled = False

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.material = Material(color=(1.0, 0.75, 0.05, 1.0), shading_model=Material.ShadingModel.EMISSIVE)
        self.material.managed = True

    def update(self) -> None:
        self.points = [
            (-0.50, 0.00, 0.50),
            (0.00, 0.00, 0.00),
            (-0.50, 0.00, -0.50)
        ]
        super().update()


class Cursor(Object3D):

    def __init__(
            self,
            label: str = "",
            detail: str = "",
            radius: float = 1.0,
            width: float = 0.02,
            text_color: Color = (1.00, 1.00, 1.00, 1.00),
            color: Color = (1.00, 1.00, 1.00, 1.00),
            custom_cursor: Optional[Object3D] = None,
            selection_chevron: bool = False,
            highlight_on_hover: bool = False
    ):
        super().__init__()

        # self._ignore_depth = True
        self._minimal = False
        self._width = width
        self._color = color
        self._highlight_on_hover = highlight_on_hover

        self._cursor = Circle(
            radius=radius,
            segments=64,
            width=2,
            color=self._color,
        )
        self._cursor.x_rotation = math.pi / 2.0
        self._cursor.managed = True
        self.add_child(self._cursor)
        self._crosshair = Crosshair(size=radius / 2, color=self._color, width=2)
        self._cursor.add_child(self._crosshair)

        # Chevron
        self._chevron = None
        self._selection_chevron = False
        self.selection_chevron = selection_chevron  # Just to trigger the setter

        self._custom_cursor = custom_cursor
        if self._custom_cursor is not None:
            self._cursor.visible = False
            self.add_child(self._custom_cursor)

        self._label = Text(
            text=label,
            text_color=text_color,
            text_scale=0.3,
            font="SourceSansPro-Bold.ttf",
            align=Text.Align.CENTER,
            shadow_x=0.01,
            shadow_y=0.01
        )
        self._label.managed = True
        self._label.z = -1.25
        self.add_child(self._label)

        self._detail = Text(
            text=detail,
            text_color=Colors.orange,
            text_scale=0.3,
            font="SourceSansPro-Bold.ttf",
            align=Text.Align.CENTER,
            shadow_x=0.01,
            shadow_y=0.01
        )
        self._detail.managed = True
        self._detail.z = -1.5
        self.add_child(self._detail)

        # First column
        self._help_function = Text(
            text="",
            text_color=Colors.black,
            text_scale=0.3,
            font="SourceSansPro-Bold.ttf",
            align=Text.Align.RIGHT,
            shadow_x=0.01,
            shadow_y=0.01
        )
        self._help_function.managed = True
        self._help_function.z = -1.75
        self._help_function.x = -0.1

        # Second column (:)
        self._help_separation = Text(
            text="",
            text_color=Colors.black,
            text_scale=0.3,
            font="SourceSansPro-Bold.ttf",
            align=Text.Align.LEFT,
            shadow_x=0.01,
            shadow_y=0.01
        )
        self._help_separation.managed = True
        self._help_separation.z = -1.75
        self._help_separation.x = 0

        # Third column
        self._help_controls = Text(
            text="",
            text_color=Colors.black,
            text_scale=0.3,
            font="SourceSansPro-Bold.ttf",
            align=Text.Align.LEFT,
            shadow_x=0.01,
            shadow_y=0.01
        )
        self._help_controls.managed = True
        self._help_controls.z = -1.75
        self._help_controls.x = 0.25

        self._input_mapping = None
        self._state_mapping = None

    @property
    def input_mapping(self) -> dict:
        return self._input_mapping

    @input_mapping.setter
    def input_mapping(self, value: dict) -> None:
        if self._input_mapping != value:
            self._input_mapping = value

    @property
    def state_mapping(self) -> dict:
        return self._state_mapping

    @state_mapping.setter
    def state_mapping(self, value: dict) -> None:
        if self._state_mapping != value:
            self._state_mapping = value
            first_help_column = ""
            second_help_column = ""
            third_help_column = ""
            for sublist in self._state_mapping:
                if sublist[2] is not None:
                    if self._input_mapping[sublist[0]] is not None:
                        first_help_column += sublist[2] + "\n"
                        second_help_column += ": \n"
                        for control in self._input_mapping[sublist[0]][1]:
                            if isinstance(control, str):
                                third_help_column += control + " "
                            else:
                                third_help_column += control[0] + "-" + control[1] + " "
                        third_help_column += "\n"

            self._help_function.text = first_help_column
            self._help_separation.text = second_help_column
            self._help_controls.text = third_help_column.replace("_", " ")

    @property
    def custom_cursor(self) -> Optional[Object3D]:
        return self._custom_cursor

    @custom_cursor.setter
    def custom_cursor(self, value):
        if self._custom_cursor != value:
            if self._custom_cursor is not None:
                self.remove_child(self._custom_cursor)
            self._custom_cursor = value
            self._cursor.visible = self._custom_cursor is None
            if self._custom_cursor is not None:
                self._custom_cursor.material.color = self.color
                self.add_child(self._custom_cursor)

    @property
    def radius(self) -> float:
        return self._cursor.radius

    @radius.setter
    def radius(self, value: float) -> None:
        if self._cursor.radius != value:
            self._cursor.radius = value
            self._crosshair.size = value / 2

    @property
    def color(self) -> Color:
        return self._color

    @color.setter
    def color(self, value: Color) -> None:
        if self._color != value:
            self._color = value
            self._set_color(self._color)

    def _set_color(self, color: Color) -> None:
        self._cursor.color = color
        self._crosshair.color = color
        if self._custom_cursor is not None:
            self._custom_cursor.material.color = color

    @property
    def label(self) -> str:
        return self._label.text

    @label.setter
    def label(self, value: str) -> None:
        if self._label.text != value:
            self._label.text = value

    @property
    def detail(self) -> str:
        return self._detail.text

    @detail.setter
    def detail(self, value: str) -> None:
        if self._detail.text != value:
            self._detail.text = value

    @property
    def minimal(self) -> bool:
        return self._minimal

    @minimal.setter
    def minimal(self, value: bool) -> None:
        if self._minimal != value:
            self._minimal = value
            self._set_minimal(self._minimal)

    def _set_minimal(self, minimal: bool) -> None:
        self.radius = 1.0 if not minimal else 0.25
        self._label.visible = not minimal
        self._detail.visible = not minimal
        if self._selection_chevron and self._chevron is not None:
            self._chevron.visible = not minimal
        if self._custom_cursor is not None:
            self._cursor.visible = minimal
            self._custom_cursor.visible = not minimal

    @property
    def highlight_on_hover(self) -> bool:
        return self._highlight_on_hover

    @highlight_on_hover.setter
    def highlight_on_hover(self, value: bool) -> None:
        if self._highlight_on_hover != value:
            self._highlight_on_hover = value

    @property
    def selection_chevron(self) -> bool:
        return self._selection_chevron

    @selection_chevron.setter
    def selection_chevron(self, value: bool) -> None:
        if self._selection_chevron != value:
            if self._selection_chevron:
                self.remove_child(self._chevron)
                self._chevron = None
            self._selection_chevron = value
            if self._selection_chevron:
                self._chevron = Chevron()
                self._chevron.managed = True
                self.add_child(self._chevron)

    def on_menu_opened(self, menu: Menu) -> None:
        self._set_minimal(True)
        self._set_color((1.0, 0.75, 0.05, 1.00))

    def on_menu_closed(self) -> None:
        self._set_minimal(self._minimal)
        self._set_color(self._color)

    def show_help(self) -> None:
        self.add_child(self._help_function)
        self.add_child(self._help_separation)
        self.add_child(self._help_controls)

    def hide_help(self) -> None:
        self.remove_child(self._help_function)
        self.remove_child(self._help_separation)
        self.remove_child(self._help_controls)

    def update_cursor(self, picker: 'Picker') -> None:
        selected = self.editor.active_object

        self.matrix = picker.cursor_matrix

        if self._highlight_on_hover:
            self._set_color((1.0, 0.75, 0.05, 1.00) if picker.picked_object is not None else self._color)

        # Chevron-Arrow pointing selected object
        if selected is not None and self._selection_chevron and self._chevron is not None:
            # Calculate angle between cursor and target to determine if we show the chevron
            selected_translation = selected.matrix_world.translation

            target = selected_translation - self.editor.matrix.translation
            angle_to_target = picker.direction_corrected.angle(target)
            minimum_angle = 2.0 * math.atan((self.radius * self.editor.ui_scale) / picker.ray_vector.length)

            if angle_to_target > minimum_angle:
                # Project target onto cursor plane
                v = selected_translation - picker.cursor_matrix.translation
                dist = v.dot(picker.direction_corrected)
                proj_target = selected_translation - dist * picker.direction_corrected

                # Find angle in cursor space, this is mostly trial and error adjustments from the Blender version code
                in_cursor_target = picker.cursor_matrix.mul_vector3(proj_target)
                in_cursor_target.y = 0.0
                cursor_angle = in_cursor_target.normalized.quaternion('X', 'Z').matrix44 * Matrix44.from_y_rotation(math.pi)

                # Draw chevron

                # Round scale, because we don't need that much precision and it throws matrix comparisons into thinking
                # that the matrices change every frame even when not changing at all
                try:
                    scale = round(math.log10(1.0 + ((angle_to_target - minimum_angle) / (math.pi / 2.0)) * 9.0), 3)
                except:
                    scale = 1.0
                # this offset is to translate the chevron outside of the cursor's outer circle
                offset = Matrix44.from_translation(Vector3((self.radius + scale + 0.1, 0.0, 0.0)))

                # We have to include the ui_scale because we're changing the world matrix
                chevron_matrix = \
                    cursor_angle * \
                    offset * \
                    Matrix44.from_scale(Vector3((scale, scale, scale)))  # * \
                # Matrix44.from_scale(Vector3((self.editor.ui_scale, self.editor.ui_scale, self.editor.ui_scale))) * \

                self._chevron.matrix = chevron_matrix
                self._chevron.visible = True
            else:
                self._chevron.visible = False
        elif self._chevron:
            self._chevron.visible = False
