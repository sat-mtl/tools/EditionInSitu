from eis.client.input import LocalInputMethod
from eis.commands.engine import LoadSceneCommand
from eis.display.components.buttons.button import Button
from eis.display.components.menus.menu import Menu
from eis.graph.scene import Scene


def reset_scene_menu(cls: Menu):

    class ResetSceneMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Reset Scene Menu")

            def _confirm(target: Button, input: LocalInputMethod) -> None:
                self.editor.machine.client.session.command(
                    LoadSceneCommand(Scene()))
                self.close()

            self.add_item("Confirm", _confirm)

            def _cancel(target: Button, input: LocalInputMethod) -> None:
                self.close()

            self.add_item("Cancel", _cancel)

    return ResetSceneMenu
