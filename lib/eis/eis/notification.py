from enum import IntEnum, unique
from typing import TYPE_CHECKING

from satnet.notification import ServerNotification, ClientNotification, Notification

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.session import EISSession
    # noinspection PyUnresolvedReferences
    from eis.client.session import EISLocalSession
    # noinspection PyUnresolvedReferences
    from eis.server.session import EISRemoteSession


@unique
class NotificationId(IntEnum):
    """
    EIS Notification Ids
    """

    # BUILT-IN
    PEER_CONNECTED = 0x01
    PEER_DISCONNECTED = 0x02
    USER_ADDED = 0x03
    USER_REMOVED = 0x04
    LOCATION = 0x05

    # VIVE
    VIVE_STATE_CLIENT = 0x30
    VIVE_STATE_PEER = 0x31


class EISNotification(Notification['EISSession']):
    """
    Client notification, can only be used by clients
    Helper wrapper to save on imports and generic type declarations in concrete eis commands
    """
    pass


class EISClientNotification(ClientNotification['EISRemoteSession']):
    """
    Client notification, can only be used by clients
    Helper wrapper to save on imports and generic type declarations in concrete eis commands
    """
    pass


class EISServerNotification(ServerNotification['EISLocalSession']):
    """
    Server notification, can only be used by servers
    Helper wrapper to save on imports and generic type declarations in concrete eis commands
    """
    pass
