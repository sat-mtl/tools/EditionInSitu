import logging
from abc import ABCMeta
from typing import Callable, Set, Type, Optional

from eis.graph.scene import Scene
from satlib.abstractstatic import abstractstatic

logger = logging.getLogger(__name__)
converters: Set[Type['FormatConverter']] = set()


def register_converter(converter: Type['FormatConverter']):
    converters.add(converter)


def converter(cls: Type['FormatConverter']) -> Callable:
    """
    Decorator used to register graph converters
    :return: Callable
    """
    register_converter(cls)
    return cls


class FormatConverter(metaclass=ABCMeta):

    @abstractstatic
    def handles(extension: str) -> bool:
        """
        Returns whether the converter handles the file extension
        :param extension: str - File extension
        :return: whether this converter supports that extension
        """

    @classmethod
    def import_path(cls, path: str) -> Optional[Scene]:
        """
        Import scene data from path
        :param path: str - Path to scene data
        :return: Scene
        """
        try:
            with open(path, 'rb') as file:
                return cls.import_data(file.read())
        except FileNotFoundError:
            logger.error("File not found ({})".format(path))

    @abstractstatic
    def import_data(data: bytes) -> Optional[Scene]:
        """
        Import scene data
        :param data: bytes - Scene data
        :return: Scene
        """

    @classmethod
    def export_path(cls, scene: Scene, path: str) -> None:
        """
        Export scene to a specific file path
        :param scene: Scene to export
        :param path: Absolute path of the main save file
        :return:
        """

    @abstractstatic
    def export_data(scene: Scene, path: str) -> bytes:
        """
        Export scene data
        :param scene: Scene
        :param path: Path
        :return: bytes - Scene data
        """
