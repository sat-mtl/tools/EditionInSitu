import os
from typing import Optional

from eis.converters import FormatConverter, converter
from eis.graph.model import Model
from eis.graph.scene import Scene
from eis.utils.paths import EISPath
from satnet.serialization import Serializable


@converter
class EISConverter(FormatConverter):

    @staticmethod
    def handles(extension: str) -> bool:
        return extension in ['eis']

    @staticmethod
    def import_data(data: bytes) -> Optional[Scene]:
        loaded = Serializable.deserialize(data)
        if isinstance(loaded, Scene):
            return loaded
        elif isinstance(loaded, Model):
            return Scene(model=loaded)
        else:
            return None

    @classmethod
    def export_path(cls, scene: Scene, path: str) -> None:
        scene_path = os.path.join(EISPath.get_scenes_path(), path)
        with open(scene_path, 'wb') as f:
            f.write(cls.export_data(scene=scene, path=scene_path))

    @staticmethod
    def export_data(scene: Scene, path: str) -> bytes:
        return scene.serialize()
