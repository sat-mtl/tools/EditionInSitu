import logging
import os
import sys

from abc import ABCMeta, abstractmethod
from time import time
from typing import Any, List, Optional, Type, Dict

from eis.cache_manager import CacheManager, Cacheable
from eis.graph.object_3d import Object3D
from satmath.matrix44 import Matrix44

logger = logging.getLogger(__name__)


class AssetSignature:
    def __init__(self, parameters: Dict[str, Any]) -> None:
        self._signature = parameters
        self._signature_hash = hash(frozenset(self._signature.items()))

    @property
    def signature_hash(self) -> int:
        return self._signature_hash


class AssetCached(Cacheable):
    def __init__(self, Object3D: 'Object3D') -> None:
        super().__init__()
        self._object_3d = Object3D

    def get_content(self) -> 'Object3D':
        return self._object_3d


class Asset(metaclass=ABCMeta):
    """
    Base class for assets
    This works as a class-based plugin, if you want to add an asset type, create a class deriving from Asset
    in the 'plugins' subfolder. This class must implement an instantiate() method and define its
    _supported_extensions variable, containing a list of lower case extensions (with the period).
    """
    _asset_types: List[Type['Asset']] = []

    def __init__(self, data: Any, path: str) -> None:
        """
        Class constructor
        :param path: str - Path of the file representing the asset, wil throw a FileNotFoundError exception
        if the path does not exist or is not a file
        :param data: Raw data buffer or handle that will be used by each plugin class
        """
        self._path = path
        self._data = data
        self._object3D: Optional[Object3D] = None
        self._changeable_shape = False  # True if the asset type can be represented by both plane or a cube

    @classmethod
    def init_plugins(cls) -> None:
        cls.find_plugins()
        cls.register_plugins()

    @classmethod
    def find_plugins(cls) -> None:
        """
        Finds all files in the plugin directory and imports them
        """
        from importlib import import_module

        plugin_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'plugins')
        plugin_files = [os.path.splitext(f)[0] for f in os.listdir(plugin_dir) if f.endswith(".py")]
        sys.path.insert(0, plugin_dir)
        for plugin in plugin_files:
            import_module(plugin)

    @classmethod
    def register_plugins(cls) -> None:
        """
        Register all class based plugins
        """
        for plugin in cls.__subclasses__():
            cls._asset_types.append(plugin)

    @classmethod
    def create_asset(cls, file: str):
        raise NotImplementedError

    @classmethod
    def factory_create_asset(cls, path: str) -> Optional['Asset']:
        raise NotImplementedError

    def step(self, now: float, dt: float) -> None:
        pass

    def on_hovered(self) -> None:
        pass

    def on_unhovered(self) -> None:
        pass

    @property
    def object3D(self) -> Optional[Object3D]:
        return self._object3D

    @object3D.setter
    def object3D(self, object3d: Object3D) -> None:
        self._object3D = object3d

    @property
    def path(self) -> str:
        return self._path

    # Make the object3D that represents this asset, must be redeclared for each asset type
    @abstractmethod
    def _make_asset_object(self, scale: float, shape: Object3D) -> Optional['Object3D']:
        pass

    # Instantiates the behaviors for this asset, and any other thing that should be done before the instantiating ends.
    def _instantiate_behaviors(self) -> None:
        pass

    def set_shape(self, shape: Object3D) -> Optional[Object3D]:
        if not self._changeable_shape:
            return None

        signature_hash = AssetSignature(parameters={'path': self._path, 'size': os.path.getsize(
            self._path), 'primitive': type(shape)}).signature_hash
        cached = CacheManager.get_cacheable(key='assets', signature=signature_hash)
        if cached:
            cached.last_used = time()
            shape = cached.get_content()
        else:
            shape.name = self.path
            shape._matrix = Matrix44.identity()
            shape.material = self._object3D.material
            shape.height = self._object3D.height
            shape.width = self._object3D.width
            shape.interactive = self._object3D.interactive
            shape.matrix = self._object3D.matrix

            if signature_hash is not None:
                CacheManager.add_cacheable(key='assets', signature=signature_hash,
                                           cacheable=AssetCached(Object3D=shape))

        for behavior in self._object3D.behaviors.copy():
            shape.add_behavior(behavior)
            self._object3D.remove_behavior(behavior)

        return shape

    def instantiate(self, scale: float, shape: Object3D) -> Object3D:
        parameters = {'path': self._path, 'size': os.path.getsize(self._path), 'primitive': type(shape)}
        signature = AssetSignature(parameters=parameters)
        cached = CacheManager.get_cacheable(key='assets', signature=signature.signature_hash)
        if cached:
            self._object3D = cached.get_content()
            cached.last_used = time()
        else:
            self._object3D = self._make_asset_object(scale=scale, shape=shape)
            if signature.signature_hash is not None:
                CacheManager.add_cacheable(key='assets', signature=signature.signature_hash,
                                           cacheable=AssetCached(Object3D=self._object3D))
            else:
                logger.warning('Could not cache primitive of type {}'.format(type(self).__name__))

        self._instantiate_behaviors()

        return self._object3D

    def object_3d_copy(self) -> Object3D:
        return self.object3D.copy()
