from typing import Any, Callable, Optional
from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState

import eis.display.components.menu_description as menu_description
import eis.display.components.menus.editor_menu as editor_menu
from eis.display.components.menus.menu_actions.editing_menus import NavigatingCursor, walk_menu
from eis.graph.behaviors.physics.physics_body_behavior import PhysicsBodyBehavior
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.vector3 import Vector3  # type: ignore


class WalkState(BaseEISState):

    def __init__(self, *args: Any, on_exit: Optional[Callable[[], None]] = None, **kwargs: Any) -> None:
        super().__init__(*args, name="walk", verb="walk", recallable=False, **kwargs)

        self._on_exit = on_exit

        # Movement values
        self._movement = Vector3()
        self._speed_multiplier = 25

        self._recall_state = False  # Whether it should recall to previous state right now or not

        self._body_behavior = None

        # Cursor
        self._cursor = lambda: NavigatingCursor(
            selection_chevron=True,
            label="Navigation",
            color=(0.55, 0.76, 0.29, 1.00)
        )

        # Menu
        menu_geometry = menu_description.MenuGeometry(self.editor.config).current_menu_geometry
        self.menu = lambda: walk_menu(editor_menu.get_menu(menu_geometry))()

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping = [("help", "_help", None),
                                ("navigate", "_stop_navigating", "Stop navigating (on release)"),
                                ("move", "_set_movement", None),
                                ("reset", "_reset_position", "Reset position and navigating speed"),
                                ("speed_up", "_speed_up", "Speed up movement"),
                                ("slow_down", "_slow_down", "Slow down movement")]

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected and self.editor.synchronized_physical_body is not None and self.editor.synchronized_physical_body.get_behavior_by_type(
            PhysicsBodyBehavior) is not None
        return should and can

    def enter(self, event: EventData) -> None:
        super().enter(event)
        self._body_behavior = self.editor.synchronized_physical_body.get_behavior_by_type(PhysicsBodyBehavior)
        assert(self._body_behavior)
        # Reset values
        self._movement = Vector3()

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self.editor.active_object = self.editor.last_active_object

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)
        increment = [self._movement[0] * dt * self._speed_multiplier,
                     self._movement[1] * dt * self._speed_multiplier, 0.0]
        self._body_behavior.increment_velocity(increment)

        if self._recall_state:
            self._back()

    def _back(self) -> None:
        self._recall_state = False
        self._body_behavior = None
        self._on_exit()

    def _set_movement(self, movement: Vector3) -> None:
        self._movement = self.machine.editor.rotation.mul_vector3(movement)

    def _reset_position(self, input: LocalInputMethod) -> None:
        self._speed_multiplier = 25
        self._movement = Vector3()
        new_matrix = Matrix44.identity()
        new_matrix.translation = self.editor.config['navigation.reset_position']
        self._body_behavior.max_velocity = 5
        self._body_behavior.reset_base_position(new_matrix.translation)
        self._body_behavior._lateral_friction = 0.9
        self.editor.matrix = new_matrix

    def _speed_up(self, input: LocalInputMethod) -> None:
        self._speed_multiplier *= 1.3
        self._body_behavior.max_velocity *= 2
        self._body_behavior._lateral_friction *= 1.5

    def _slow_down(self, input: LocalInputMethod) -> None:
        self._speed_multiplier /= 1.3
        self._body_behavior.max_velocity /= 2
        self._body_behavior._lateral_friction /= 1.5

    def _stop_navigating(self, input: LocalInputMethod) -> None:
        # Using a boolean and recalling the state in "run" instead of here because if the client is laggy, it is possible
        # to reach a situation where we are still in the navigating state with the navigating button unpressed because the
        # previous state in state_machine hasn't updated yet. Calling the recall in run prevents this.
        if not input[1]:
            self._recall_state = True
