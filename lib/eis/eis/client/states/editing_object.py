import logging
from transitions.core import EventData  # type: ignore
from typing import Any, Optional, Tuple

import eis.display.components.menu_description as menu_description
import eis.display.components.menus.editor_menu as editor_menu
from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.display.components.menus.menu_actions.editing_menus import editing_object_menu

logger = logging.getLogger(__name__)


class EditingObjectState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="editing_object", verb="edit_object", recallable=True, **kwargs)

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=True,
            highlight_on_hover=False,
            label="select",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        menu_geometry = menu_description.MenuGeometry(self.editor.config).current_menu_geometry
        self.menu = lambda: editing_object_menu(editor_menu.get_menu(menu_geometry))()

        # Quick transformations
        self._quick_translating = False
        self._quick_rotating = False
        self._quick_scaling = False

        # Variables
        self._root_object: Optional[Object3D] = None

        # Whether the previous state is navigating
        self.from_navigating = False

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend(
            [("navigate", "_navigate", "Navigate (while pressed)"),
             ("select", "_select", "Select an object"),
             ("undo", "_undo", "Undo"),
             ("redo", "_redo", "Redo")]
        )

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        if (event.state.name == "fly" or event.state.name == "walk"):
            self.from_navigating = True
        can = self.client.connected and (self.editor.active_object is not None or (
            self._root_object is not None and self.from_navigating))

        return should and can

    def enter(self, event: EventData) -> None:
        # Should happen before super to be available for the cursor in super()
        super().enter(event)
        if self.from_navigating:
            self.from_navigating = False
        else:
            self._root_object = self.editor.last_active_object

    def exit(self, event: EventData) -> None:
        super().exit(event)
        if (event.event.name == "fly" or event.event.name == "walk"):
            return
        self.editor.active_object = self.editor.last_active_object

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)

    def _navigate(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()

    def _select(self, args: Tuple[LocalInputMethod, bool]) -> None:
        # If the button is unpressed
        if not args[1]:
            return

        assert(self._root_object is not None)
        result = args[0].picker.pick_in(self._root_object, False)
        if result is not None:
            active_object = self.editor.active_object
            object3D = result[0]
            if active_object is object3D:
                self.editor.active_object = None
            else:
                self.editor.active_object = object3D

    def _undo(self, input: LocalInputMethod) -> None:
        pass

    def _redo(self, input: LocalInputMethod) -> None:
        pass
