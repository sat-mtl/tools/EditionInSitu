from typing import Any, Optional

from transitions.core import EventData  # type: ignore

from eis.actions.transform_object import TransformObjectAction
from eis.client.states.transform import BaseTransformState
from eis.commands.transform_offset import TransformOffsetCommand
from eis.graph.helpers.scale import ScaleHelper
from satmath.vector3 import Vector3
from satmath.matrix44 import Matrix44


class ScalingState(BaseTransformState):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="scaling", **kwargs)
        self._helper = ScaleHelper()

        self._first_distance_received = True
        self._initial_distance = 1.0
        self._current_distance = 1.0

    def enter(self, event: EventData) -> None:
        super().enter(event)
        assert(self.editor.last_active_object is not None)

        self._current_distance = 2.0

    def _set_mapping(self) -> None:
        super()._set_mapping()
        if self._is_quick:
            self._inputs_mapping += [("quick_scale", "_release", "Release to stop transforming")]
        self._inputs_mapping += [("distance", "_update_distance", "The distance between pointers changes the scale")]

    def exit(self, event: EventData) -> None:

        super().exit(event)

        final_matrix = self.editor.last_active_object.matrix_offset * self.editor.last_active_object.matrix
        self._machine.client.session.action(TransformObjectAction(
            object=self.editor.last_active_object, matrix=final_matrix))

        for passive_object in self.editor.passive_objects:
            final_matrix = passive_object.matrix_offset * passive_object.matrix
            self._machine.client.session.action(TransformObjectAction(
                object=passive_object, matrix=final_matrix))

        self._first_distance_received = True
        self._initial_distance = 1.0
        self._current_distance = 1.0

    def run(self, now: float, dt: float) -> None:
        assert(self.editor.last_active_object is not None)

        super().run(now=now, dt=dt)
        if self._first_distance_received:
            return

        scale = self._current_distance / self._initial_distance
        scaling_matrix = self.editor.last_active_object.matrix * \
            Matrix44.from_scale((scale, scale, scale)) * self.editor.last_active_object.matrix.inverse
        self.editor.last_active_object.matrix_offset = scaling_matrix
        self._machine.client.session.command(TransformOffsetCommand(
            object=self.editor.last_active_object, matrix=scaling_matrix))

        for passive_object in self.editor.passive_objects:
            scaling_matrix = passive_object.matrix * \
                Matrix44.from_scale((scale, scale, scale)) * passive_object.matrix.inverse
            passive_object.matrix_offset = scaling_matrix
            self._machine.client.session.command(TransformOffsetCommand(
                object=passive_object, matrix=scaling_matrix))

    def _update_distance(self, input: Any) -> None:
        distance = input[1]

        if self._first_distance_received:
            self._initial_distance = distance
            self._first_distance_received = False

        self._current_distance = distance
