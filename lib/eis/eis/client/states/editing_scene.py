import logging
from transitions.core import EventData  # type: ignore
from typing import Any, Optional, Tuple

from eis.actions.engine import RemoveObject3DAction
from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.commands.animation import StartTimelineCommand, StopTimelineCommand, ResetTimelineCommand
from eis.display.components.cursor import Cursor
from eis.display.components.menus.menu_actions.editing_scene_menu import editing_scene_menu
import eis.display.components.menu_description as menu_description
import eis.display.components.menus.editor_menu as editor_menu
from eis.graph.behaviors.highlight_behavior import HighlightBehavior
from eis.graph.object_3d import Object3D
from satnet.commands.actions import RedoCommand, UndoCommand

logger = logging.getLogger(__name__)


class EditingSceneState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="editing_scene", verb="edit_scene", recallable=True, **kwargs)

        self._selection_lock = False  # Whether the selection is enabled or not, True = disabled, False = enabled
        self._added_objects_layer: Optional[Object3D] = None
        # Keeps the selection between successive activation of this state
        self._previously_selected_object: Optional[Object3D] = None

        # Timeline movement
        self._previous_timeline_movement_position = 0
        self._move_timeline_enabled = False
        self._timeline_movement = 0
        self._timeline_velocity = 0
        self._timeline_time = self.editor.timeline.time

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=True,
            highlight_on_hover=False,
            label="select",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        menu_geometry = menu_description.MenuGeometry(self.editor.config).current_menu_geometry
        self.menu = lambda: editing_scene_menu(editor_menu.get_menu(menu_geometry))()

        # Quick transformations
        self._quick_translating = False
        self._quick_rotating = False
        self._quick_scaling = False

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("navigate", "_navigate", "Navigate (while pressed)"),
                                     ("select", "_select", "Select an object"),
                                     ("alternative_select", "_alternative_select", "Select multiple objects (hold)"),
                                     ("quick_translate", "_quick_translate", "Translate (hold)"),
                                     ("quick_rotate", "_quick_rotate", "Rotate (hold)"),
                                     ("quick_scale", "_quick_scale", "Scale (hold)"),
                                     ("axis_z", "_navigate_timeline", "Navigate within the timeline"),
                                     ("undo", "_undo", "Undo"),
                                     ("redo", "_redo", "Redo"),
                                     ("toggle_timeline", "_toggle_timeline", "Toggle timeline")])

        self._highlighted_objects: Set[Object3D] = set()
        self._highlight_behavior_speed = 2.0

    @property
    def selection_lock(self) -> bool:
        return self._selection_lock

    # locks/unlock selection/hovering (will unhighlight hovered items if locking)
    @selection_lock.setter
    def selection_lock(self, value: bool) -> None:
        if self._selection_lock != value:
            self._selection_lock = value

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    def enter(self, event: EventData) -> None:
        # Should happen before super to be available for the cursor in super()
        super().enter(event)
        self._selection_lock = False
        selection = [child for child in self.machine.editor.scene.model.root.children if child.name == "Added_objects_layer"]
        if selection:
            self._added_objects_layer = selection[0]
        self.only_pick_assets = not self.machine.editor.config['input.pick_in_scene']

        if self.machine.editor.delete_active_object:
            self.editor.machine.client.session.action(RemoveObject3DAction(self._previously_selected_object))
        else:
            self.editor.active_object = self._previously_selected_object

    def exit(self, event: EventData) -> None:
        super().exit(event)

        for object in self._highlighted_objects:
            object.remove_behavior_by_type(HighlightBehavior)
        self._highlighted_objects.clear()
        self._previously_selected_object = self.editor.active_object
        self.editor.active_object = None

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)
        time_point = self._machine.editor.timeline.time
        time_string = "t={:.2f}".format(time_point)

        for user in self.editor.users.values():
            for input_method in user.input_methods.values():
                input_method.cursor.label = "t={:.2f}".format(time_point)
                if self.editor.active_object is not None:
                    input_method.cursor.label = f"selected - {time_string} - {self.editor.active_object.name}"
                else:
                    input_method.cursor.label = f"select - {time_string}"

        # Detect which object is pointed at
        newly_picked_object: Set[Object3D] = set()
        if not self._selection_lock:
            for user in self.editor.users.values():
                for input_method in user.input_methods.values():
                    if self.only_pick_assets:
                        result = input_method.picker.pick_in(self._added_objects_layer, True)
                    else:
                        result = input_method.picker.pick()
                    if result:
                        picked_object, position = result
                        if picked_object is not None:
                            picked_name = picked_object.name
                            if len(picked_name) > 32:
                                picked_name = f"{picked_name[:15]}...{picked_name[-15:]}"
                            input_method.cursor.label = f"select - {time_string} - {picked_name}"

                            if picked_object is not self.editor.active_object \
                                    and picked_object not in self.editor.passive_objects:
                                newly_picked_object.add(picked_object)

        # Highlight objects pointed at, if they are not already selected
        for object in self._highlighted_objects:
            if object not in newly_picked_object:
                object.remove_behavior_by_type(HighlightBehavior)
        for object in newly_picked_object:
            if object not in self._highlighted_objects:
                object.add_behavior(HighlightBehavior(speed=self._highlight_behavior_speed))
        self._highlighted_objects = newly_picked_object

        # Movement
        if self._move_timeline_enabled:
            # Move the timeline cursor according to the velocity
            self._timeline_time += self._timeline_movement
            self.editor.machine.client.session.command(ResetTimelineCommand(timeline_position=self._timeline_time))
            self._timeline_movement = 0

    def _quick_translate(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if self.editor.active_object is not None and not self.editor.menu_manager.menu_open:
            if args[1]:
                self._machine.translate(input=args[0], quick=True)

    def _quick_rotate(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if self.editor.active_object is not None and not self.editor.menu_manager.menu_open:
            if args[1]:
                self._machine.rotate(input=args[0], quick=True)

    def _quick_scale(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if self.editor.active_object is not None and not self.editor.menu_manager.menu_open:
            if args[1]:
                self._machine.scale(input=args[0], quick=True)

    def _select(self, args: Tuple[LocalInputMethod, bool]) -> None:
        """
        Select a single object. If a multiple selection was already active, unselect everything
        to select only the targeted object.
        """
        if not self._selection_lock and args[0].picker is not None and args[1]:  # if pressed
            if self.only_pick_assets:
                assert(self._added_objects_layer is not None)
                result = args[0].picker.pick_in(self._added_objects_layer, True)
            else:
                result = args[0].picker.pick()

            if result[0] is not None:
                active_object = self.editor.active_object
                passive_objects = self.editor.passive_objects
                object3D = result[0]
                assert(object3D is not None)

                if object3D in self._highlighted_objects:
                    self._highlighted_objects.remove(object3D)

                if active_object is object3D:
                    if passive_objects:
                        self.editor.clear_passive_objects()
                    else:
                        self.editor.active_object = None
                else:
                    object3D.remove_behavior_by_type(HighlightBehavior)
                    self.editor.clear_passive_objects()
                    self.editor.active_object = object3D

    def _alternative_select(self, args: Tuple[LocalInputMethod, bool]) -> None:
        """
        Selects multiple objects through the editor.active_object and editor.passive_objects properties
        """
        if not self._selection_lock and args[1]:  # if pressed
            if self.only_pick_assets:
                assert(self._added_objects_layer is not None)
                result = args[0].picker.pick_in(self._added_objects_layer, True)
            else:
                result = args[0].picker.pick()

            if result[0] is not None:
                active_object = self.editor.active_object
                passive_objects = self.editor.passive_objects
                object3D = result[0]
                assert(object3D is not None)

                if object3D in self._highlighted_objects:
                    object3D.remove_behavior_by_type(HighlightBehavior)
                    self._highlighted_objects.remove(object3D)

                # Object is already active object
                if active_object is object3D:
                    if passive_objects:
                        new_active_object = passive_objects[0]
                        self.editor.remove_passive_object(new_active_object)
                        self.editor.active_object = new_active_object
                    else:
                        self.editor.active_object = None
                # Object isn't active object, but exists
                elif active_object is not None:
                    # Object isn't a passive object
                    if object3D not in passive_objects:
                        self.editor.add_passive_object(object3D)
                    # Ojbect is a passive object
                    else:
                        self.editor.remove_passive_object(object3D)
                        self.editor.add_passive_object(active_object)
                        self.editor.active_object = object3D
                # There is no active object
                else:
                    self.editor.active_object = object3D

    def _navigate(self, args: LocalInputMethod) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()

    def _navigate_timeline(self, args: Tuple[LocalInputMethod, float, int]) -> None:
        if args[2] and self.editor.menu_manager.menu_open:
            if self._move_timeline_enabled:
                if args[2] == 4:
                    self._move_timeline_enabled = False
                else:
                    # This is because the trackpads records 0.0 values from time to time for no good reason whatsoever
                    # It notably happens the last value before disabling, and so we have to ignore them.
                    if args[1] != 0.0:
                        self._timeline_movement = (args[1] - self._previous_timeline_movement_position) * 5.0
                        self._previous_timeline_movement_position = args[1]

            elif args[2] == 3:
                self._move_timeline_enabled = True
                self._timeline_time = self.editor.timeline.time
                self._timeline_velocity = 0
                self._previous_timeline_movement_position = args[1]

    def _toggle_timeline(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if not self.editor.menu_manager.menu_open:
            if args[1]:
                if self.editor.timeline.running:
                    self.editor.machine.client.session.command(StopTimelineCommand())
                    self.editor.timeline.stop()
                else:
                    self.editor.machine.client.session.command(StartTimelineCommand())
                    self.editor.timeline.start()

    def _undo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(UndoCommand())

    def _redo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(RedoCommand())
