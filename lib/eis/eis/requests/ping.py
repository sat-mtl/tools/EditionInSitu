import logging
import time
from typing import Optional, TYPE_CHECKING

from eis.request import RequestId, ResponseId, EISClientRequest, EISServerResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from eis.client.session import EISLocalSession
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.PING)
class Ping(EISClientRequest):
    """
    Ping request
    """

    _fields = ['ping']

    def __init__(self) -> None:
        super().__init__()
        self.ping = time.time()

    def handle(self, session: 'EISRemoteSession', respond: ResponseCallback) -> None:
        logger.debug("Received ping request from {}".format(session))
        respond(Pong(self.ping, time.time()))


@response(id=ResponseId.PONG)
class Pong(EISServerResponse[Ping]):

    _fields = ['ping', 'pong']

    def __init__(self, ping: Optional[float]=None, pong: Optional[float]=None) -> None:
        super().__init__()
        self.ping = ping
        self.pong = pong

    def handle(self, request: Ping, session: 'EISLocalSession') -> None:
        assert isinstance(request, Ping)
        logger.debug("Received pong response")
        if self.ping is not None and self.pong is not None:
            session.editor.clock.update(self.ping, self.pong)
        else:
            logger.warning("Pong did not contain timestamps")
