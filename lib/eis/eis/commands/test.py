import logging
from typing import TYPE_CHECKING

from eis.command import CommandId, EISClientCommand
from satnet.command import command
from eis.graph.object_3d import Object3D

if TYPE_CHECKING:
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@command(id=CommandId.TEST)
class TestCommand(EISClientCommand):
    _fields = []

    def __init__(self) -> None:
        super().__init__()

    def handle(self, session: 'EISRemoteSession') -> None:
        obj = Object3D()
        obj.visible = False
        session.editor.scene.add_child(obj)
