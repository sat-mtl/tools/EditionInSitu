import logging
import os
from typing import Optional, TYPE_CHECKING

from eis.command import CommandId, EISCommand
from eis.entity import Sync, SyncEntity
from eis.graph.animation_curve import AnimationCurve
from eis.graph.behavior import Behavior
from eis.graph.event_handler import EventHandler
from eis.graph.material import Material
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from eis.graph.texture import Texture
from eis.utils.paths import EISPath
from satnet.command import command
from satnet.serialization import Serializable

if TYPE_CHECKING:
    from eis.session import EISSession

logger = logging.getLogger(__name__)


@command(id=CommandId.ADD_MODEL)
class AddModelCommand(EISCommand):
    _fields = ['model']

    def __init__(self, model: Optional[Model] = None) -> None:
        super().__init__()
        self.model = model

    def handle(self, session: 'EISSession') -> None:
        if self.model is not None:
            session.editor.add_model(model=self.model)


@command(id=CommandId.ADD_OBJECT3D)
class AddObject3DCommand(EISCommand):
    _fields = ['object', 'parent_id']

    def __init__(self, object: Optional[Object3D] = None) -> None:
        super().__init__()
        self.object = object
        self.parent_id = object.parent.uuid if object and object.parent else None

    def handle(self, session: 'EISSession') -> None:
        if self.object is None:
            logger.warning("No object to add in command.")
            return

        parent = None
        if self.parent_id:
            parent = session.editor.scene.model.get_object_by_uuid(self.parent_id)
            if not parent:
                logger.warning(
                    "Could not find parent id \"{}\" when adding object \"{}\"".format(self.parent_id, self.object))
        if not parent:
            parent = session.editor.scene.model.root

        parent.add_child(self.object)


@command(id=CommandId.REMOVE_OBJECT3D)
class RemoveObject3DCommand(EISCommand):
    _fields = ['object_id']

    def __init__(self, object: Optional[Object3D] = None) -> None:
        super().__init__()
        self.object_id = object.uuid if object else None

    def handle(self, session: 'EISSession') -> None:
        if self.object_id is None:
            logger.warning("No object id to remove in command")
            return

        object = session.editor.scene.model.get_object_by_uuid(self.object_id)
        if not object:
            logger.warning("Could not find object id \"{}\" for removal".format(self.object_id))
            return

        object.remove()


@command(id=CommandId.ADD_ANIMATION_CURVE)
class AddAnimationCurveCommand(EISCommand):
    _fields = ['animation_curve']

    def __init__(self, animation_curve: Optional[AnimationCurve] = None) -> None:
        super().__init__()
        self.animation_curve = animation_curve

    def handle(self, session: 'EISSession') -> None:
        if self.animation_curve is None:
            logger.warning("No animation curve to add in command.")
            return

        session.editor.scene.model.add_animation_curve(self.animation_curve)


@command(id=CommandId.REMOVE_ANIMATION_CURVE)
class RemoveAnimationCurveCommand(EISCommand):
    _fields = ['animation_curve_id']

    def __init__(self, animation_curve: Optional[AnimationCurve] = None) -> None:
        super().__init__()
        self.animation_curve_id = animation_curve.uuid if animation_curve else None

    def handle(self, session: 'EISSession') -> None:
        if self.animation_curve_id is None:
            logger.warning("No animation curve id to remove in command")
            return
        animation_curve = session.editor.scene.model.get_animation_curve_by_uuid(self.animation_curve_id)
        if not animation_curve:
            logger.warning("Could not find animation curve id \"{}\" for removal".format(self.animation_curve_id))
            return
        session.editor.scene.model.remove_animation_curve(animation_curve)


@command(id=CommandId.ADD_BEHAVIOR)
class AddBehaviorCommand(EISCommand):
    _fields = ['behavior', 'object_id']

    def __init__(self, behavior: Optional[Behavior] = None) -> None:
        super().__init__()
        self.behavior = behavior
        self.object_id = behavior.graphbase.uuid if behavior and behavior.graphbase else None

    def handle(self, session: 'EISSession') -> None:
        if self.behavior is None:
            logger.warning("No behavior to add in command.")
            return

        object = None
        if self.object_id:
            object = session.editor.scene.model.get_entity_by_uuid(self.object_id)
            if not object:
                logger.warning(
                    "Could not find object id \"{}\" when adding behavior \"{}\"".format(self.object_id, self.behavior))
        if not object:
            object = session.editor.scene.model.root

        object.add_behavior(self.behavior)


@command(id=CommandId.REMOVE_BEHAVIOR)
class RemoveBehaviorCommand(EISCommand):
    _fields = ['behavior_id']

    def __init__(self, behavior: Optional[Behavior] = None) -> None:
        super().__init__()
        self.behavior_id = behavior.uuid if behavior else None

    def handle(self, session: 'EISSession') -> None:
        if self.behavior_id is None:
            logger.warning("No behavior id to remove in command")
            return
        behavior = session.editor.scene.model.get_behavior_by_uuid(self.behavior_id)
        if not behavior:
            logger.warning("Could not find behavior id \"{}\" for removal".format(self.behavior_id))
            return
        if behavior.graphbase:
            behavior.graphbase.remove_behavior(behavior)
        else:
            session.editor.scene.model.remove_behavior(behavior)


@command(id=CommandId.REMOVE_BEHAVIOR_BY_TYPE)
class RemoveBehaviorByTypeCommand(EISCommand):
    _fields = ['object_id', 'behavior_type']

    def __init__(self, object: Optional[Object3D] = None, behavior_type: Optional[str] = None) -> None:
        super().__init__()
        self.object_id = object.uuid if object else None
        self.behavior_type = behavior_type

    def handle(self, session: 'EISSession') -> None:
        object = None
        if self.object_id:
            object = session.editor.scene.model.get_object_by_uuid(self.object_id)
            if not object:
                logger.warning("Could not find object id \"{}\" when removing behavior \"{}\"".format(
                    self.object_id, self.behavior_type))
        if not object:
            return

        if self.behavior_type is None:
            logger.warning("Invalid behavior type given to remove behavior by type..")
            return

        object.remove_behavior_by_type(behavior_type=Behavior.behaviors[self.behavior_type])


@command(id=CommandId.ADD_EVENT_HANDLER)
class AddEventHandlerCommand(EISCommand):
    _fields = ['key', 'behavior', 'event_handler']

    def __init__(self,
                 key: Optional[str] = None,
                 behavior: Optional[Behavior] = None,
                 event_handler: Optional[EventHandler] = None) -> None:
        super().__init__()
        self.key = key
        self.behavior = behavior
        self.event_handler = event_handler

    def handle(self, session: 'EISSession') -> None:
        if not self.key or not self.behavior or not self.event_handler:
            logger.warning('Received an AddEventHandler command with invalid arguments.')
            return

        session.editor.scene.model.add_event_handler(
            behavior=self.behavior, key=self.key, event_handler=self.event_handler)


@command(id=CommandId.REMOVE_EVENT_HANDLER)
class RemoveEventHandlerCommand(EISCommand):
    _fields = ['key', 'behavior']

    def __init__(self, behavior: Optional[Behavior] = None, key: Optional[str] = None) -> None:
        super().__init__()
        self.key = key
        self.behavior = behavior

    def handle(self, session: 'EISSession') -> None:
        session.editor.scene.model.remove_event_handler(behavior=self.behavior, key=self.key)


@command(id=CommandId.ADD_MATERIAL)
class AddMaterialCommand(EISCommand):
    _fields = ['material']

    def __init__(self, material: Optional[Material] = None) -> None:
        super().__init__()
        self.material = material

    def handle(self, session: 'EISSession') -> None:
        if self.material is None:
            logger.warning("No material to add in command.")
            return

        session.editor.scene.model.add_material(self.material, used=False)


@command(id=CommandId.REMOVE_MATERIAL)
class RemoveMaterialCommand(EISCommand):
    _fields = ['material_id']

    def __init__(self, material: Optional[Material] = None) -> None:
        super().__init__()
        self.material_id = material.uuid if material else None

    def handle(self, session: 'EISSession') -> None:
        if self.material_id is None:
            logger.warning("No material id to remove in command")
            return

        material = session.editor.scene.model.get_material_by_uuid(self.material_id)
        if not material:
            logger.warning("Could not find material id \"{}\" for removal".format(self.material_id))
            return

        session.editor.scene.model.remove_material(material)


@command(id=CommandId.ADD_TEXTURE)
class AddTextureCommand(EISCommand):
    _fields = ['texture']

    def __init__(self, texture: Optional[Texture] = None) -> None:
        super().__init__()
        self.texture = texture

    def handle(self, session: 'EISSession') -> None:
        if self.texture is None:
            logger.warning("No texture to add in command.")
            return

        session.editor.scene.model.add_texture(self.texture)


@command(id=CommandId.REMOVE_TEXTURE)
class RemoveTextureCommand(EISCommand):
    _fields = ['texture_id']

    def __init__(self, texture: Optional[Texture] = None) -> None:
        super().__init__()
        self.texture_id = texture.uuid if texture else None

    def handle(self, session: 'EISSession') -> None:
        if self.texture_id is None:
            logger.warning("No texture id to remove in command")
            return

        texture = session.editor.scene.model.get_texture_by_uuid(self.texture_id)
        if not texture:
            logger.warning("Could not find texture id \"{}\" for removal".format(self.texture_id))
            return
        session.editor.scene.model.remove_texture(texture)


@command(id=CommandId.ADD_MESH)
class AddMeshCommand(EISCommand):
    _fields = ['mesh']

    def __init__(self, mesh: Optional[Mesh] = None) -> None:
        super().__init__()
        self.mesh = mesh

    def handle(self, session: 'EISSession') -> None:
        if self.mesh is None:
            logger.warning("No mesh to add in command.")
            return

        session.editor.scene.model.add_mesh(self.mesh, used=False)


@command(id=CommandId.REMOVE_MESH)
class RemoveMeshCommand(EISCommand):
    _fields = ['mesh_id']

    def __init__(self, mesh: Optional[Mesh] = None) -> None:
        super().__init__()
        self.mesh_id = mesh.uuid if mesh else None

    def handle(self, session: 'EISSession') -> None:
        if self.mesh_id is None:
            logger.warning("No mesh id to remove in command")
            return

        mesh = session.editor.scene.model.get_mesh_by_uuid(self.mesh_id)
        if not mesh:
            logger.warning("Could not find mesh id \"{}\" for removal".format(self.mesh_id))
            return

        session.editor.scene.model.remove_mesh(mesh)


@command(id=CommandId.PROPERTY_CHANGED)
class PropertyChangedCommand(EISCommand):
    _fields = ['entity_id', 'sync_name', 'value']

    def __init__(
            self,
            entity: Optional[SyncEntity] = None,
            sync: Optional[Sync] = None
    ) -> None:
        super().__init__()
        self.entity_id = entity.uuid if entity else None
        self.sync_name = sync.name if sync else None
        self.value = sync.get(entity) if sync and entity else None

    def handle(self, session: 'EISSession') -> None:
        if self.entity_id is None:
            logger.warning("No sync entity id in command")
            return

        if self.sync_name is None:
            logger.warning("No sync name in command")
            return

        entity = session.editor.scene.model.get_entity_by_uuid(self.entity_id)
        if not entity:
            logger.warning(
                "Could not find entity id \"{}\" for syncing of \"{}\" property".format(self.entity_id, self.sync_name))
            return

        sync = entity.get_sync_by_name(self.sync_name)
        if not sync:
            logger.warning("Could not find sync \"{}\" in \"{}\"".format(self.sync_name, entity))
            return

        # Set value silently, we don't want to recreate another synchronization cycle
        sync.set_silently(entity, self.value)


@command(id=CommandId.SAVE_SCENE_FILE)
class SaveSceneFileCommand(EISCommand):
    _fields = ['path']

    def __init__(
            self, path: Optional[str] = None
    ) -> None:
        super().__init__()
        self.path = path

    def handle(self, session: 'EISSession') -> None:
        logger.debug("Saving scene...")
        session.editor.save_scene_to_file(path=self.path)


@command(id=CommandId.LOAD_SCENE_FILE)
class LoadSceneFileCommand(EISCommand):
    _fields = ['scene_name']

    def __init__(
            self,
            scene_name: Optional[str] = None
    ) -> None:
        super().__init__()
        self.scene_name = scene_name

    def handle(self, session: 'EISSession') -> None:
        if not self.scene_name:
            logger.warning('No scene name provided for loading')
            return

        with open(os.path.join(EISPath.get_scenes_path(), self.scene_name), 'rb') as f:
            scene_data = Serializable.deserialize(f.read())
            if scene_data:
                session.editor.load_scene(scene=scene_data)


@command(id=CommandId.LOAD_SCENE)
class LoadSceneCommand(EISCommand):
    _fields = ['scene']

    def __init__(
            self,
            scene: Optional[Scene] = None
    ) -> None:
        super().__init__()
        self.scene = scene

    def handle(self, session: 'EISSession') -> None:
        if not self.scene:
            logger.warning('No scene provided for loading')
            return

        session.editor.load_scene(scene=self.scene)
