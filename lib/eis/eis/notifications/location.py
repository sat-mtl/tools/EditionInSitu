import logging
from typing import Optional, TYPE_CHECKING

from eis.notification import NotificationId, EISServerNotification
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satnet.notification import notification

if TYPE_CHECKING:
    from eis.session import EISSession
    from eis.client.session import EISLocalSession

logger = logging.getLogger(__name__)


@notification(id=NotificationId.LOCATION)
class Location(EISServerNotification):
    _fields = ['peer_id', 'location', 'rotation']

    def __init__(
            self,
            session: Optional['EISSession'] = None,
            location: Optional[Vector3] = None,
            rotation: Optional[Quaternion] = None
    ) -> None:
        super().__init__()
        self.peer_id = session.id if session else None
        self.location = location
        self.rotation = rotation

    def handle(self, session: 'EISLocalSession') -> None:
        # if self.context is None:
        #     logger.warning("Context not found!")
        #     return

        if self.peer_id is None:
            logger.warning("No peer id!")
            return

        peer = session.peers.get(self.peer_id)
        if not peer:
            logger.warning("Peer not found!")
            return

        if self.location is not None:
            peer.location = self.location
        if self.rotation is not None:
            peer.rotation = self.rotation
