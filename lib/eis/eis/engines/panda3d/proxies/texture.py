import logging
from typing import Optional, TYPE_CHECKING

from panda3d import core

from eis.engine import Engine, proxy
from eis.engines.panda3d.engine import Panda3DEngine
from eis.engines.panda3d.proxies.base import Panda3DGraphProxyBase
from eis.graph.texture import Texture, TextureProxy

logger = logging.getLogger(__name__)


@proxy(Panda3DEngine, Texture)
class Panda3DTextureProxy(TextureProxy, Panda3DGraphProxyBase[core.Texture]):
    def __init__(self, engine: Engine, proxied: Texture) -> None:
        super().__init__(engine=engine, proxied=proxied)
        self._texture: Optional[core.Texture] = None
        self._size_x = 0
        self._size_y = 0
        self._format = ""
        self._compression = Texture.Compression.NONE
        self._timestamp = 0.0

    @property
    def texture(self) -> Optional[core.Texture]:
        return self._texture

    def make(self) -> None:
        super().make()

        if not self._texture:
            self._texture = core.Texture()

        if self._proxied.compression == self._proxied.Compression.DXT1:
            self._texture.setup_2d_texture(self._proxied.size_x, self._proxied.size_y,
                                           core.Texture.T_unsigned_byte, core.Texture.F_rgb8)
            self._texture.set_compression(core.Texture.CM_dxt1)
            self._texture.set_ram_image(self._proxied.data)
        elif self._proxied.compression == self._proxied.Compression.DXT5:
            self._texture.setup_2d_texture(self._proxied.size_x, self._proxied.size_y,
                                           core.Texture.T_unsigned_byte, core.Texture.F_rgba8)
            self._texture.set_compression(core.Texture.CM_dxt5)
            self._texture.set_ram_image(self._proxied.data)
        elif self._proxied.compression == self._proxied.Compression.NONE:
            if self._proxied.format in ['rgba', 'bgra']:
                texture_format = core.Texture.F_rgba8
            elif self._proxied.format in ['rgb', 'bgr']:
                texture_format = core.Texture.F_rgb8
            else:
                logger.warning(f"Unsupported image format: {self._proxied.format}")
                return

            if self._proxied.component_type == self._proxied.ComponentType.UNSIGNED_BYTE:
                self._texture.setup_2d_texture(self._proxied.size_x, self._proxied.size_y,
                                               core.Texture.T_unsigned_byte, texture_format)
            elif self._proxied.component_type == self._proxied.ComponentType.UNSIGNED_SHORT:
                self._texture.setup_2d_texture(self._proxied.size_x, self._proxied.size_y,
                                               core.Texture.T_unsigned_short, texture_format)
            elif self._proxied.component_type == self._proxied.ComponentType.FLOAT:
                self._texture.setup_2d_texture(self._proxied.size_x, self._proxied.size_y,
                                               core.Texture.T_float, texture_format)
            else:
                logger.warning(f"Unknown component type: {self._proxied.component_type}")
                return

            self._texture.set_compression(core.Texture.CM_off)
            self._texture.set_ram_image_as(self._proxied.data, self._proxied.format.upper())
        else:
            logger.warning("Unsupported texture compression {}".format(self._proxied.compression))
            return

        self._size_x = self._proxied.size_x
        self._size_y = self._proxied.size_y
        self._format = self._proxied.format
        self._compression = self._proxied.compression
        self._timestamp = self._proxied.timestamp

        self._texture.name = self._proxied.name
        self._texture.set_magfilter(core.Texture.FT_linear_mipmap_linear)
        self._texture.set_minfilter(core.Texture.FT_linear_mipmap_linear)
        self._texture.set_anisotropic_degree(16)

    def update(self) -> None:
        super().update()

        proxied = self._proxied
        if (self._size_x != proxied._size_x or self._size_y != proxied.size_y or
                self._format != proxied.format or self._compression != proxied.compression):
            self.make()

        if self._timestamp != proxied.timestamp:
            if self._compression == Texture.Compression.DXT1:
                self._texture.set_ram_image(self._proxied.data)
            elif self._compression == Texture.Compression.DXT5:
                self._texture.set_ram_image(self._proxied.data)
            elif self._compression == Texture.Compression.NONE and self._format[0:3] == 'bgr':
                self._texture.set_ram_image(self._proxied.data)
            elif self._compression == Texture.Compression.NONE and self._format[0:3] == 'rgb':
                self._texture.set_ram_image_as(self._proxied.data, self._format.upper())
            else:
                logger.warning("Unsupported texture format and compression: {}, {}".format(
                    self._format, self._compression))
