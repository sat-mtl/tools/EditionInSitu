"""
Panda3d Engine Implementation
"""

# Import proxies so that they register with the system first
from eis.engines.panda3d.proxies.geometry import Panda3DGeometryProxy
from eis.engines.panda3d.proxies.light import Panda3DLightProxy
from eis.engines.panda3d.proxies.material import Panda3DMaterialProxy
from eis.engines.panda3d.proxies.mesh import Panda3DMeshProxy
from eis.engines.panda3d.proxies.object_3d import Panda3DObject3DProxy
from eis.engines.panda3d.proxies.primitives.shape import Panda3DShapeProxy
from eis.engines.panda3d.proxies.text import Panda3DTextProxy
from eis.engines.panda3d.proxies.texture import Panda3DTextureProxy
