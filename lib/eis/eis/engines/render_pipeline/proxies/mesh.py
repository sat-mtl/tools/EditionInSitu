import logging
from typing import Optional

from panda3d import core

from eis.engine import Engine, proxy
from eis.engines.render_pipeline.engine import RenderPipelineEngine
from eis.engines.render_pipeline.proxies.base import RenderPipelineGraphProxyBase
from eis.engines.render_pipeline.proxies.geometry import RenderPipelineGeometryProxy
from eis.graph.mesh import Mesh, MeshProxy

logger = logging.getLogger(__name__)


@proxy(RenderPipelineEngine, Mesh)
class RenderPipelineMeshProxy(MeshProxy, RenderPipelineGraphProxyBase[core.GeomNode]):
    def __init__(self, engine: Engine, proxied: Mesh) -> None:
        super().__init__(engine=engine, proxied=proxied)
        self._geom_node: Optional[core.GeomNode] = None

    @property
    def geom_node(self) -> Optional[core.GeomNode]:
        return self._geom_node

    def _update_geometry(self) -> None:
        for geometry in self._proxied.geometries:
            geometry.update_proxy(engine=self._engine)
            assert isinstance(geometry.proxy, RenderPipelineGeometryProxy)
            self._geom_node.add_geom(geometry.proxy.geom, geometry.proxy.state)

    def update_graph(self) -> None:
        if self._proxied.dirty:
            self.make()
        super().update_graph()

        self._geom_node.remove_all_geoms()
        self._update_geometry()

        for obj3d_proxy in [obj3d.proxy for obj3d in self.proxied.objects3d]:
            obj3d_proxy.update_graph()

    def make(self) -> None:
        super().make()
        self._geom_node = core.GeomNode(self._proxied.name)
        self._update_geometry()
