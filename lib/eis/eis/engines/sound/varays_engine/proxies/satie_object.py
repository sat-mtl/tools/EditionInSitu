import logging
from typing import Any, TypeVar

from eis.editor_support.osc_manager import OscManager
from eis.engine import Engine, proxy
from eis.engines.sound.proxies.sound_object import SoundEngineSoundObjectProxy
from eis.engines.sound.varays_engine.engine import VaraysEngine
from eis.graph.sound_objects.satie_object import SatieObject
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

O = TypeVar('O', bound='VaraysSatieObject')

logger = logging.getLogger(__name__)


@proxy(VaraysEngine, SatieObject)
class VaraysEngineSatieObjectProxy(SoundEngineSoundObjectProxy):
    def __init__(self, engine: Engine, proxied: O) -> None:
        assert isinstance(engine, VaraysEngine)
        SoundEngineSoundObjectProxy.__init__(self, engine=engine, proxied=proxied)
        logger.warning("Satie object proxies for the Varays Engine not implemented yet")

    def _create_sound(self, position: Vector3) -> None:
        pass

    def _remove_sound(self) -> None:
        pass

    def remove(self) -> None:
        self._remove_sound()
        super().remove()

    def dispose(self) -> None:
        self._remove_sound()
        super().dispose()

    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        super().set_matrix(matrix, matrix_offset)
