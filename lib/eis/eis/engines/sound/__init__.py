"""
Sound Engine implementation
"""

# Import proxies so that they register with the system first
from eis.engines.sound.proxies.sound_object import SoundEngineSoundObjectProxy

from eis.engines.sound.satie_engine.proxies.sound_object import SatieEngineSoundObjectProxy
from eis.engines.sound.varays_engine.proxies.sound_object import VaraysEngineSoundObjectProxy

from eis.engines.sound.satie_engine.proxies.video_object import SatieEngineVideoObjectProxy
from eis.engines.sound.varays_engine.proxies.video_object import VaraysEngineVideoObjectProxy

from eis.engines.sound.satie_engine.proxies.satie_object import SatieEngineSatieObjectProxy
from eis.engines.sound.varays_engine.proxies.satie_object import VaraysEngineSatieObjectProxy