import logging
from typing import List, Generic, TypeVar

from eis.engine import Engine, proxy
from eis.engines.sound.engine import SoundEngine
from eis.engines.sound.proxies.base import SoundGraphProxyBase
from eis.graph.sound_object import SoundObject, SoundObjectProxy
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

O = TypeVar('O', bound='SoundObject')

logger = logging.getLogger(__name__)


@proxy(SoundEngine, SoundObject)
class SoundEngineSoundObjectProxy(Generic[O], SoundObjectProxy[O], SoundGraphProxyBase):
    def __init__(self, engine: Engine, proxied: O) -> None:
        assert isinstance(engine, SoundEngine)
        SoundObjectProxy.__init__(self, engine=engine, proxied=proxied)

    @property
    def bound_box(self) -> List[Vector3]:
        return [Vector3(), Vector3()]

    def add_child(self, child: O) -> None:
        if not child.proxy:
            logger.error("Trying to add a child ({}) without a proxy".format(child))
            return

        assert isinstance(child.proxy, SoundEngineSoundObjectProxy)

    def gui(self, gui: bool) -> None:
        pass

    def remove_child(self, child: O) -> None:
        if child.proxy:
            assert isinstance(child.proxy, SoundEngineSoundObjectProxy)
