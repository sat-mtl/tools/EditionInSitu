from eis.engine import Engine, proxy
from eis.engines.dummy.engine import DummyEngine
from eis.engines.dummy.proxies.base import DummyGraphProxyBase
from eis.graph.material import Material, MaterialProxy


@proxy(DummyEngine, Material)
class DummyMaterialProxy(MaterialProxy, DummyGraphProxyBase):
    def __init__(self, engine: Engine, proxied: Material) -> None:
        super().__init__(engine=engine, proxied=proxied)
