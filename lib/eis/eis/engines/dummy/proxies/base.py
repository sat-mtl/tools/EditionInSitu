from eis.engines.dummy.engine import DummyEngine


class DummyGraphProxyBase:
    def __init__(self, engine: DummyEngine) -> None:
        assert isinstance(engine, DummyEngine)
        self._dummy = engine

    @property
    def dummy(self) -> DummyEngine:
        return self._dummy
