import logging
from typing import List, Tuple
from uuid import UUID

from eis.picker import Picker
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)


class DummyPicker(Picker['DummyEngine']):
    """
    Dummy picker
    """

    def __init__(self, engine: 'DummyEngine', editor: 'ClientEditor') -> None:
        super().__init__(engine=engine, editor=editor)

    def update_rays(self) -> None:
        pass

    def _pick(self) -> List[Tuple[UUID, Vector3]]:
        return []
