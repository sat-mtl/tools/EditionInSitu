"""
Dummy Engine implementation, does nothing but printing its state
"""

# Import proxies so that they register with the system first
from eis.engines.dummy.proxies.geometry import DummyGeometryProxy
from eis.engines.dummy.proxies.light import DummyLightProxy
from eis.engines.dummy.proxies.material import DummyMaterialProxy
from eis.engines.dummy.proxies.mesh import DummyMeshProxy
from eis.engines.dummy.proxies.object_3d import DummyObject3DProxy
from eis.engines.dummy.proxies.primitives.shape import DummyShapeProxy
from eis.engines.dummy.proxies.text import DummyTextProxy
from eis.engines.dummy.proxies.texture import DummyTextureProxy
