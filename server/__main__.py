"""
EIS Server Application

This is the main file, it sets up the environment before launching the application
"""

import os
import sys

# Add paths to local libs
path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/py-satlib")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/py-satmath")))
# sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/py-satmath/build/lib.linux-x86_64-3.6")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/py-satnet")))
# sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/py-satnet/build/lib.linux-x86_64-3.6")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../lib/eis")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/render_pipeline")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/glTF-Blender-IO/addons")))

# Command line parsing
from argparse import ArgumentParser

parser = ArgumentParser(description="EIS Server")
parser.add_argument("-i", "--interactive", action="store_true", help="Launches in REPL mode")
parser.add_argument("-c", "--config", help="Additional configuration file")
parser.add_argument("-s", "--scene", help="Load scene")
parser.add_argument("-p", "--project", help="Project-specific configuration file")
parser.add_argument("-P", "--profile", action='store_true',
                    help="Show profiling information after exiting the application")
args = parser.parse_args()

# Setup logger
import logging
from logging import StreamHandler
from satlib.formatter import SATFormatter

handler = StreamHandler(stream=sys.stdout)
# handler.level = logging.DEBUG
handler.setFormatter(SATFormatter())
logging.basicConfig(level=logging.DEBUG, handlers=[handler])
logging.getLogger('satnet.adapters').setLevel(logging.INFO)
logging.getLogger('eis.requests.ping').setLevel(logging.INFO)
if args.interactive:
    # If interactive, shut up annoying logging
    logging.getLogger('satnet.adapters').setLevel(logging.INFO)
    logging.getLogger('eis.requests.ping').setLevel(logging.INFO)

# Check PYTHONOPTIMIZE flag is not set.
if sys.flags.optimize in [1,2]:
    logging.error((
        "The PYTHONOPTIMIZE option is incompatible with EiS, please remove "
        "the option and try again."
    ))
    sys.exit(1)


def main() -> None:
    """
    Application's main method
    :return:
    """

    import eis.constants
    eis.constants.SERVER = True

    from ServerApplication import ServerApplication
    try:
        ServerApplication(repl=args.interactive, scene=args.scene, config=args.config,
                          project=args.project, profile=args.profile).run()
    except KeyboardInterrupt:
        sys.exit(0)


# Allow launching from the package directly
if __name__ == "__main__":
    main()
