import os
import sys
import cProfile, pstats, io  # For the profiler (when needed), do not remove

# Add paths to local libs
path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../lib/satlib")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../lib/satnet")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../lib/eis")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/render_pipeline")))

# Setup logger
import time
import logging
from logging import StreamHandler
from satlib.formatter import SATFormatter

handler = StreamHandler(stream=sys.stdout)
handler.setFormatter(SATFormatter())
logging.basicConfig(level=logging.DEBUG, handlers=[handler])

from eis.engines.panda3d.engine import Panda3DEngine
from eis.graph.model import Model

engine = Panda3DEngine()
engine._headless = True
engine.initialize()

# path = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../res/scenes/Bretez/export/Bretez.bam"))
path = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../res/scenes/Sponza/export/Sponza.bam"))
# path = os.path.realpath("/home/ubald/tmp/cubes.bam")

pr = cProfile.Profile()
pr.enable()

running = True

def loaded(model:Model):
    print("loaded", model)
    running = False

model = engine.read_model_from_file_async(path=path, callback=loaded)
print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
# engine.add_model(model)

while running:
    engine.step(0, 0)
    time.sleep(0.01)

pr.disable()
s = io.StringIO()
ps = pstats.Stats(pr, stream=s).sort_stats("ncalls")
ps.print_stats()
print(s.getvalue())
