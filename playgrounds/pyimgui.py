import os
import sys
# Add paths to local libs
path = os.path.dirname(os.path.realpath(__file__))
print(os.path.realpath(os.path.join(path, "../extlib/pyimgui")))
sys.path.insert(0, os.path.realpath(os.path.join(path, "../extlib/pyimgui")))

import OpenGL.GL as gl
import imgui

from direct.showbase.ShowBase import ShowBase



class Main(ShowBase):
    def __init__(self):
        # Initialize the ShowBase class from which we inherit, which will
        # create a window and set up everything we need for rendering into it.
        ShowBase.__init__(self)

        size = 800, 600

        io = imgui.get_io()
        print("AAA")
        print(io)
        print(io.fonts)
        print(io.fonts.add_font_default)
        io.fonts.add_font_default()
        print("DSDADASDAD")
        io.display_size = size

        self.loop_mask = taskMgr.add(self.loop, "loop")

    def loop(self, task):
        imgui.new_frame()

        if imgui.begin_main_menu_bar():
            if imgui.begin_menu("File", True):

                clicked_quit, selected_quit = imgui.menu_item(
                    "Quit", 'Cmd+Q', False, True
                )

                if clicked_quit:
                    exit(1)

                imgui.end_menu()
            imgui.end_main_menu_bar()

        imgui.show_test_window()

        imgui.begin("Custom window", True)
        imgui.text("Bar")
        imgui.text_colored("Eggs", 0.2, 1., 0.)
        imgui.end()

        imgui.render()


if __name__ == "__main__":
    main = Main()
    main.run()
    imgui.shutdown()
