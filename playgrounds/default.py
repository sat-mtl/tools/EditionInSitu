from typing import Generic, TypeVar

T = TypeVar('T')

class A(Generic[T]):
    def __init__(self, a:T):
        self.a = a
        print(isinstance(a, T))

class B:
    pass

class C:
    pass

ab = A[B](B())

print(isinstance(ab.a, B))