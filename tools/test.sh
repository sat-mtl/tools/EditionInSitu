#!/bin/bash
set -e

if [ ! -d "./env" ]; then
  echo "Python virtual environment has not been setup it."
  echo "Please run ./setup.py."
  exit 1
fi

venv_activated=0
if [ -z "${VIRTUAL_ENV}" ]; then
  echo Activating virtual environment
  source ./env/bin/activate
  venv_activated=1
fi

python3 -m unittest discover -s lib/eis/tests -t lib/eis

if [ "${venv_activated}" = 1 ]; then
  echo Deactivating virtual environment
  deactivate
fi
